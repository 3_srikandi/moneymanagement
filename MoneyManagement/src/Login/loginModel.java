package Login;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;
import moneymanagement.MoneyManagement;

/**
 *
 * @author Ofri Cantika Valent
 */
public class loginModel {
    MoneyManagement connect = new MoneyManagement();
    ResultSet rs = null;
    PreparedStatement ps = null;
    
    
    
    
    public boolean cekUsernamePassword(String username,String password) throws SQLException
    {
        
        Connection conn = null;
        conn = connect.connect();
        String sql = "select * from user where username =? and password =?";
        System.out.println(password);
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,username);
            ps.setString(2,password);
            rs = ps.executeQuery();

         
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
        conn.close();
        return rs.next();
    } 
   
     public void sessionUser(String username){
         Connection conn = null;
         conn = connect.connect();
         String sql = "INSERT INTO sessionUser(username) VALUES(?)";
         try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,username);
            ps.executeUpdate();
            conn.close();
         }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "con not insert to session user");
        }
     }
}