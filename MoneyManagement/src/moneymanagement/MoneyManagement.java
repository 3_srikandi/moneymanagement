/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moneymanagement;

import Login.loginView;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Cyndy Alisia
 */
public class MoneyManagement {

    public Connection connect(){
       //Class.forName("org.sqlite.JDBC");
        String url = "jdbc:sqlite:F:/projek/Money Management/management.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url); 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    public static void main(String[] args) {
        loginView theLoginView = new  loginView();
        theLoginView.setVisible(true);

    }
    
}
