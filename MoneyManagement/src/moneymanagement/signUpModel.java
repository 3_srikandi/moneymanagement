
package moneymanagement;

import static java.lang.System.out;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author Ofri Cantika Valent
 */
public class signUpModel {
    private String username;
    private String password;
   
    MoneyManagement connect = new MoneyManagement();
    
    
    private static String byteArrToString(byte[] b){
        String res = null;
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++){
           int j = b[i] & 0xff;
           if (j < 16) {
              sb.append('0');
           }
           sb.append(Integer.toHexString(j));
        }
        res = sb.toString();
        return res.toUpperCase();
    }
    
    public signUpModel(String username, String password){
        String passwordMD5 = null;
        try {                
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] tmp = password.getBytes();
            md5.update(tmp);
            passwordMD5 = byteArrToString(md5.digest());
        } catch (NoSuchAlgorithmException ex) {
            showMessageDialog(null, ex.getMessage());
        }
        this.username = username;
        this.password = passwordMD5;
    }
    public void insert() {
        String sql = "INSERT INTO user(username, password) values(?,?)";

        try( Connection conn = connect.connect(); PreparedStatement kalimat = conn.prepareStatement(sql)){            
            kalimat.setString(1, username);
            kalimat.setString(2, password);
            kalimat.executeUpdate();
            JOptionPane.showMessageDialog(null,"Data succesfully added");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Data Lost");
        }
    }
  
    public boolean checkInDatabase() throws ClassNotFoundException{
        boolean exist = false; //sama
        String query = "SELECT count(*) as total FROM user WHERE username = ? and password = ?";
       
        try( Connection conn = connect.connect();
            PreparedStatement statement = conn.prepareStatement(query);
            ){
            
            statement.setString(1,username);
            statement.setString(2, password);
            try (ResultSet result = statement.executeQuery())
            {
                result.next();
                    if(result.getInt("total")>0){
                    exist = true; //username diisi sudah ada di database
                    
                    }else{
                    exist = false; //username diisi sudah belum ada di database 
                }
              
            }
            conn.close();
        }catch(SQLException ex){
            out.println(ex);
        }
        return exist;
    }
}
