
package cashIn;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import moneymanagement.MoneyManagement;

public class routineCashInModel {
    private String description;
    private int money;
    private String firstDate;
    private String lastDate;
    private String user;
    private int iddata;

    ResultSet rs = null;
    MoneyManagement connect = new MoneyManagement();
    
    public routineCashInModel(){}
    public routineCashInModel(String des, int money, String startDate, String endDate){
        this.description = des;
        this.money = money;
        this.firstDate = startDate;
        this.lastDate = endDate;
    }
    public String userName(){
    String dataUser = null;
        try(Connection conn = connect.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            
//            username.close();
           conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser ggl");
        }
        return dataUser;
 }
    public void insert(){
        String userData = userName();
        try(Connection conn = connect.connect()){
            String sqlQuery = "INSERT INTO incomeRutin(startDate, endDate, description, money, username) values(?,?,?,?,?)";
            try( PreparedStatement sentence = conn.prepareStatement(sqlQuery)){              
                sentence.setString(1, this.firstDate);
                sentence.setString(2, this.lastDate);
                sentence.setString(3, this.description);
                sentence.setInt(4, this.money);
                sentence.setString(5, userData);
                sentence.executeUpdate();
                 JOptionPane.showMessageDialog(null, "Data is saved"); 
                 
            }
            conn.close();
        }catch(Exception e){
                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
  
 
    public void deleteSessionUser() {

        String userData = userName() ;
        try(Connection conn = connect.connect()){
            String sql = "DELETE FROM sessionUser WHERE username = '"+userData+"'";
            try(PreparedStatement st = conn.prepareStatement(sql)){
                st.executeUpdate();
            }catch(Exception e){
//                JOptionPane.showMessageDialog(null, "can not delete model");
            }
            conn.close();
        }catch(Exception e){
//                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }

    public void deleteData(int idData) {
        System.out.println(idData+firstDate+lastDate+description+money);
        String sql = "DELETE FROM incomeRutin WHERE id = '"+idData+"'";
        try(Connection conn = connect.connect(); PreparedStatement st = conn.prepareStatement(sql)){
            st.executeUpdate();
        }catch(Exception e){
            System.out.println(e+"delete model ggl");
        }
    }
    
    public void editData(int idData) {
        String userName = userName();
        System.out.println(idData+firstDate+lastDate+description+money);
        String sql = "UPDATE incomeRutin SET startDate = ?, endDate = ? , description = ?  , money = ?  WHERE id = ? ";
        try(Connection conn = connect.connect()){
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, this.firstDate);
            st.setString(2, this.lastDate);
            st.setString(3, this.description);
            st.setInt(4, this.money);
            st.setInt(5, idData);
            st.executeUpdate();
        }catch(Exception e){
           System.out.println(e.getMessage());
        }
    }
}