package cashIn;

import javax.swing.JOptionPane;

public class unroutineCashInController {
    private String description;
    private int money;
    private String startDate;
    private unroutineCashInModel newData;
    
    public unroutineCashInController(String des, int money, String startDate){
        this.description = des;
        this.money = money;
        this.startDate = startDate;
        
        newData = new unroutineCashInModel(des,money,startDate);
    }
    
    public unroutineCashInController(){}
    
    public void inserttoDb(){
        try{  
           newData.insert();        
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to access!");
        }
    }
    
    public void delete(int idData){
         try{
            newData.deleteData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
    
    public void edit(int idData){
         try{
            newData.editData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to edit controller");
        }
    }
    
    public void hapusUser(){
        unroutineCashInModel sendUser = new unroutineCashInModel();
        try{
            sendUser.deleteSessionUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
    
    public String namaUSer(){
        unroutineCashInModel sendUser = new unroutineCashInModel();
        try {
            String name = sendUser.userName();
            return name;
        }catch(Exception e){}
        return "";
    }
    
}
