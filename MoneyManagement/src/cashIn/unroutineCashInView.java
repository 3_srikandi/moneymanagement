package cashIn;

import java.awt.Color;
import Login.loginView;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import moneymanagement.MoneyManagement;
import homePage.homePageView;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class unroutineCashInView extends javax.swing.JFrame {
     private DefaultTableModel tabelModel;
     MoneyManagement connect = new MoneyManagement();
     private int idData;
     
     
    public unroutineCashInView() {
        initComponents();
       
        String name = userName();
        lbl_nama_user.setText(name);
        String[] title = {"Start_date","Description","Total_Income"};
        tabelModel = new DefaultTableModel(title, 0);
        detailTable.setModel(tabelModel);
        show();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        logoM = new javax.swing.JLabel();
        logoOney = new javax.swing.JLabel();
        unrountineincome = new javax.swing.JLabel();
        startdate = new javax.swing.JLabel();
        startDate = new com.toedter.calendar.JDateChooser();
        description = new javax.swing.JLabel();
        descriptionText = new java.awt.TextArea();
        income = new javax.swing.JLabel();
        incomeTotal = new javax.swing.JTextField();
        addbutton = new javax.swing.JButton();
        editbutton = new javax.swing.JButton();
        deletebutton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        detailTable = new javax.swing.JTable();
        detailLabel = new javax.swing.JLabel();
        logoM2 = new javax.swing.JLabel();
        logoAnagement = new javax.swing.JLabel();
        lbl_nama_user = new javax.swing.JLabel();
        btn_logout = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel4.setBackground(new java.awt.Color(153, 255, 173));

        jPanel2.setBackground(new java.awt.Color(153, 255, 173));

        logoM.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        logoM.setForeground(new java.awt.Color(0, 159, 92));
        logoM.setText("M");
        logoM.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoMMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoMMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoMMouseExited(evt);
            }
        });

        logoOney.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        logoOney.setText("oney");
        logoOney.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoOneyMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoOneyMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoOneyMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(logoM)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(logoOney)
                .addGap(0, 0, 0))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(logoM, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(logoOney))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        unrountineincome.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        unrountineincome.setText("Unroutine Income");

        startdate.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        startdate.setText("Start date       :");

        startDate.setDateFormatString("yyyy-MMM-d");

        description.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        description.setText("Description       :");

        income.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        income.setText("Total Income    :");

        addbutton.setBackground(new java.awt.Color(1, 169, 182));
        addbutton.setFont(new java.awt.Font("Comic Sans MS", 1, 12)); // NOI18N
        addbutton.setText("Add");
        addbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addbuttonActionPerformed(evt);
            }
        });

        editbutton.setBackground(new java.awt.Color(1, 169, 182));
        editbutton.setFont(new java.awt.Font("Comic Sans MS", 1, 12)); // NOI18N
        editbutton.setText("Edit");
        editbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editbuttonActionPerformed(evt);
            }
        });

        deletebutton.setBackground(new java.awt.Color(1, 169, 182));
        deletebutton.setFont(new java.awt.Font("Comic Sans MS", 1, 12)); // NOI18N
        deletebutton.setText("Delete");
        deletebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletebuttonActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(153, 255, 173));

        detailTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Start Date", "Description", "Total Income"
            }
        ));
        detailTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                detailTableMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(detailTable);

        detailLabel.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        detailLabel.setText("Unrountine Income Detail");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 703, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(detailLabel)
                        .addGap(275, 275, 275))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(detailLabel)
                .addGap(8, 8, 8)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                .addContainerGap())
        );

        logoM2.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        logoM2.setForeground(new java.awt.Color(255, 102, 51));
        logoM2.setText("M");
        logoM2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoM2MouseClicked(evt);
            }
        });

        logoAnagement.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        logoAnagement.setText("anagement");
        logoAnagement.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoAnagementMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoAnagementMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoAnagementMouseExited(evt);
            }
        });

        lbl_nama_user.setFont(new java.awt.Font("Sylfaen", 1, 24)); // NOI18N

        btn_logout.setBackground(new java.awt.Color(0, 255, 153));
        btn_logout.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        btn_logout.setText("Logout");
        btn_logout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_logoutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_logoutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_logoutMouseExited(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Sylfaen", 1, 18)); // NOI18N
        jLabel5.setText("Hi, ");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(258, 258, 258)
                        .addComponent(unrountineincome)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(startdate)
                                .addGap(18, 18, 18)
                                .addComponent(startDate, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(income))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(incomeTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(descriptionText, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(addbutton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(editbutton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(deletebutton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))))))))
                .addGap(127, 127, 127))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 720, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(48, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(logoM2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(logoAnagement)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(81, 81, 81)
                .addComponent(btn_logout)
                .addGap(39, 39, 39))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(logoM2, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(logoAnagement))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_logout)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(unrountineincome)))
                .addGap(77, 77, 77)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(startdate, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(startDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(description)
                            .addComponent(descriptionText, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(income)
                            .addComponent(incomeTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(addbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(editbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(deletebutton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void logoOneyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoOneyMouseClicked
        // TODO add your handling code here:
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoOneyMouseClicked

    private void logoMMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoMMouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoMMouseClicked

    private void logoM2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoM2MouseClicked
        // TODO add your handling code here:
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoM2MouseClicked

    private void logoAnagementMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoAnagementMouseClicked
        // TODO add your handling code here:
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoAnagementMouseClicked

  
    private void addbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addbuttonActionPerformed
        boolean total = false;
        DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
        Date now = new Date();
        
        
        String startString = df1.format(now);
        try {
            now = df1.parse(startString);
        }catch (ParseException ex) {
            
        }
        
        if(((JTextField)startDate.getDateEditor().getUiComponent()).getText().equals("") && descriptionText.getText().equals("") && incomeTotal.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Make sure all required !");  
        }
        else if(((JTextField)startDate.getDateEditor().getUiComponent()).getText().equals("")){
            JOptionPane.showMessageDialog(null, "Start date can not empty!");
        }
        else if(descriptionText.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Description can not empty!");
        }
        else if(incomeTotal.getText().equals("")){
             JOptionPane.showMessageDialog(null, "Total Income can not empty!");
        }
        else if(startDate.getDate().compareTo(now) < 0){
            JOptionPane.showMessageDialog(null, "Start date can not be smaller or same with the current date");
        }
        else{
            try{
                String desc = descriptionText.getText();
                String start =((JTextField)startDate.getDateEditor().getUiComponent()).getText();
                int money =  Integer.parseInt(incomeTotal.getText());
                unroutineCashInController newData = new unroutineCashInController(desc,money,start);
                newData.inserttoDb();
               
                descriptionText.setText("");
                ((JTextField)startDate.getDateEditor().getUiComponent()).setText("");
                incomeTotal.setText("");
                show();
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, "Data lost");
            }
        }
                 
    }//GEN-LAST:event_addbuttonActionPerformed

    private void editbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editbuttonActionPerformed
        try{  
            String desc = descriptionText.getText();
            String start =((JTextField)startDate.getDateEditor().getUiComponent()).getText();
            int money =  Integer.parseInt(incomeTotal.getText());
            unroutineCashInController dataEdited = new unroutineCashInController(desc, money, start);
            dataEdited.edit(idData);
               
            JOptionPane.showMessageDialog(null,"EDITED!");
            descriptionText.setText("");
            ((JTextField)startDate.getDateEditor().getUiComponent()).setText("");
            incomeTotal.setText("");
            show();
        }catch(Exception e){
             System.out.println(e);         
        }
    }//GEN-LAST:event_editbuttonActionPerformed

    private void deletebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletebuttonActionPerformed
         try{
            String desc = descriptionText.getText();
            String start =((JTextField)startDate.getDateEditor().getUiComponent()).getText();
            int money =  Integer.parseInt(incomeTotal.getText());
            unroutineCashInController datadeleted = new unroutineCashInController(desc, money, start);
            datadeleted.delete(idData);
               
            JOptionPane.showMessageDialog(null,"DELETED!");
            descriptionText.setText("");
            ((JTextField)startDate.getDateEditor().getUiComponent()).setText("");
            incomeTotal.setText("");
            show();
        }catch(Exception e){
            System.out.println(e);         
        }
    }//GEN-LAST:event_deletebuttonActionPerformed
    
    private void detailTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_detailTableMouseClicked
        DefaultTableModel model = (DefaultTableModel)detailTable.getModel();
        int row = detailTable.getSelectedRow();
       
        ((JTextField)startDate.getDateEditor().getUiComponent()).setText(model.getValueAt(row,0).toString());
        descriptionText.setText(model.getValueAt(row,1).toString());
        incomeTotal.setText(model.getValueAt(row,2).toString());
        
        String desc = descriptionText.getText();
        String start =((JTextField)startDate.getDateEditor().getUiComponent()).getText();
        int money =  Integer.parseInt(incomeTotal.getText());
       
       String user = userName();
        try(Connection conn = connect.connect()){ 
            ResultSet IDUSER = conn.createStatement().executeQuery("SELECT id FROM unroutineIncome WHERE startDate = '"+start+"'  and description = '"+desc+"'  and totalUang = '"+money+"' and username = '"+user+"'");
            while( IDUSER.next()){
                idData = IDUSER.getInt("id");
            }   
        }catch(Exception e){
            System.out.println(e+"id view gagal ");
        }
    }//GEN-LAST:event_detailTableMouseClicked

    private void logoOneyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoOneyMouseEntered
        logoOney.setForeground(Color.white);
        logoAnagement.setForeground(Color.white);
    }//GEN-LAST:event_logoOneyMouseEntered

    private void logoOneyMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoOneyMouseExited
        logoOney.setForeground(Color.black);
        logoAnagement.setForeground(Color.black);
    }//GEN-LAST:event_logoOneyMouseExited

    private void logoAnagementMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoAnagementMouseEntered
        logoOney.setForeground(Color.white);
        logoAnagement.setForeground(Color.white);
    }//GEN-LAST:event_logoAnagementMouseEntered

    private void logoAnagementMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoAnagementMouseExited
        logoOney.setForeground(Color.black);
        logoAnagement.setForeground(Color.black);
    }//GEN-LAST:event_logoAnagementMouseExited

    private void logoMMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoMMouseEntered
        logoOney.setForeground(Color.white);
        logoAnagement.setForeground(Color.white);
    }//GEN-LAST:event_logoMMouseEntered

    private void logoMMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoMMouseExited
        logoOney.setForeground(Color.black);
        logoAnagement.setForeground(Color.black);
    }//GEN-LAST:event_logoMMouseExited

    private void btn_logoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_logoutMouseClicked
        unroutineCashInController deleteuser = new unroutineCashInController();
        try{
            deleteuser.hapusUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "can not logout");
        }
        new loginView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btn_logoutMouseClicked

    private void btn_logoutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_logoutMouseEntered
        btn_logout.setForeground(Color.blue);
    }//GEN-LAST:event_btn_logoutMouseEntered

    private void btn_logoutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_logoutMouseExited
        btn_logout.setForeground(Color.black);
    }//GEN-LAST:event_btn_logoutMouseExited

    public String userName(){
        String userData = null;
        try(Connection conn = connect.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            userData = username.getString("username");
            conn.close();
        }catch(Exception e){
            System.out.println(e+"nmuser view ggl");
        }
        return userData;
    }                                    
   
     public void show() {
        int row = detailTable.getRowCount();
        for(int s = 0; s<row;s++){
            tabelModel.removeRow(0);
        }
        unroutineCashInController userActiv = new unroutineCashInController();
        String user = userActiv.namaUSer();
        try{
            Connection conn = connect.connect();
            ResultSet rs = conn.createStatement().executeQuery("select startDate,description, totalUang from unroutineIncome where username= '"+user+"'");
                while(rs.next()){
                    String[] data ={rs.getString(1),rs.getString(2),rs.getString(3)};
                    tabelModel.addRow(data);
                }
            }catch(SQLException ex){}
    }
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new unroutineCashInView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addbutton;
    private javax.swing.JLabel btn_logout;
    private javax.swing.JButton deletebutton;
    private javax.swing.JLabel description;
    private java.awt.TextArea descriptionText;
    private javax.swing.JLabel detailLabel;
    private javax.swing.JTable detailTable;
    private javax.swing.JButton editbutton;
    private javax.swing.JLabel income;
    private javax.swing.JTextField incomeTotal;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lbl_nama_user;
    private javax.swing.JLabel logoAnagement;
    private javax.swing.JLabel logoM;
    private javax.swing.JLabel logoM2;
    private javax.swing.JLabel logoOney;
    private com.toedter.calendar.JDateChooser startDate;
    private javax.swing.JLabel startdate;
    private javax.swing.JLabel unrountineincome;
    // End of variables declaration//GEN-END:variables
}
