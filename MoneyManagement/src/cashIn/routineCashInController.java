package cashIn;

import javax.swing.JOptionPane;

public class routineCashInController {
    private String description;
    private int money;
    private String startDate;
    private String endDate;
    private routineCashInModel newData;
    
    public routineCashInController(){}
    
    public routineCashInController(String des, int money, String startDate, String endDate){
        this.description = des;
        this.money = money;
        this.startDate = startDate;
        this.endDate = endDate;
        newData = new routineCashInModel(des,money,startDate, endDate);
    }
    
    public void inserttoDb(){
        try{  
            newData.insert();                      
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to access!");
        }
    }
    
    public void deleteUser(){
        routineCashInModel sendUser = new routineCashInModel();
        try{
            sendUser.deleteSessionUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
     
    public String userName(){
        routineCashInModel sendUser = new routineCashInModel();
        try {
            String nama = sendUser.userName();
            return nama;
        }catch(Exception e){
             
        }
        return "";
    }
     
    public void delete(int idData){
         try{
            newData.deleteData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
     
    public void edit(int idData){
         try{
            newData.editData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to edit controller");
        }
    }
}