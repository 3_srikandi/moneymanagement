package cashIn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import moneymanagement.MoneyManagement;

public class unroutineCashInModel {
    private String description;
    private int uang;
    private String firstDate;
   
    MoneyManagement connect = new MoneyManagement();
    
    public unroutineCashInModel(){}
    
    public unroutineCashInModel(String des, int money, String startDate){
        this.description = des;
        this.uang = money;
        this.firstDate = startDate;
    }
    
     public void insert(){
        String userData;
        try(Connection conn = connect.connect()){
            ResultSet username = conn.createStatement().executeQuery("select username from sessionUser");
            username.next();
            userData = username.getString("username");
            String sqlQuery = "INSERT INTO unroutineIncome(startDate, description,totalUang, username) values(?,?,?,?)";
            
            try( PreparedStatement kalimat = conn.prepareStatement(sqlQuery)){              
                 kalimat.setString(1, this.firstDate);
                 kalimat.setString(2, this.description);
                 kalimat.setInt(3, this.uang);
                 kalimat.setString(4, userData);
                 kalimat.executeUpdate();
                 JOptionPane.showMessageDialog(null, "Data is saved");  
            }

        }catch(Exception e){
                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
     
     public void deleteSessionUser() {
      
        String userData;
        try(Connection conn = connect.connect()){
            ResultSet username = conn.createStatement().executeQuery("select username from sessionUser");
            username.next();
            userData = username.getString("username");
            System.out.println(userData);
            String sql = "DELETE FROM sessionUser WHERE username = '"+userData+"'";
           
            try(PreparedStatement st = conn.prepareStatement(sql)){
                st.executeUpdate();
                JOptionPane.showMessageDialog(null, "can delete model");   
            }catch(Exception e){}

        }catch(Exception e){
//                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
     
    public String userName(){
        String userData = null;
        try(Connection conn = connect.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            userData = username.getString("username");
            conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser ggl");
        }
        return userData;
        
    }
    
    public void deleteData(int idData) {
    
        System.out.println(idData+firstDate+description+uang);
        String sql = "DELETE FROM unroutineIncome WHERE id = '"+idData+"'";
    
        try(Connection conn = connect.connect(); PreparedStatement st = conn.prepareStatement(sql)){
            st.executeUpdate();
        }catch(Exception e){
            System.out.println(e+"delete model ggl");
        }     
    }
    public void editData(int idData) {
        String userName = userName();
        System.out.println(idData+firstDate+description+uang);
        String sql = "UPDATE unroutineIncome SET startDate = ?, description = ?  , totalUang = ?  WHERE id = ? ";
          
        try(Connection conn = connect.connect()){
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, this.firstDate);
            st.setString(2, this.description);
            st.setInt(3, this.uang);
            st.setInt(4, idData);
            st.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
