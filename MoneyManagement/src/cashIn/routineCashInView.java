package cashIn;

import java.awt.Color;
import Login.loginView;
import homePage.homePageView;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import moneymanagement.MoneyManagement;
import java.util.Date;

public class routineCashInView extends javax.swing.JFrame {
    private DefaultTableModel tabel;
    MoneyManagement connect = new MoneyManagement();
    private int idData;

    public routineCashInView(){
        initComponents();
        String name = namaUser();
        lbl_nama_user.setText(name);
        String[] title = {"Start_date","End_date","Description","Total_Income"};
        tabel = new DefaultTableModel(title, 0);
        tableData.setModel(tabel);
        show();
    }
   
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        startDate = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        endDate = new com.toedter.calendar.JDateChooser();
        jLabel9 = new javax.swing.JLabel();
        description = new java.awt.TextArea();
        jLabel10 = new javax.swing.JLabel();
        routineIncome = new javax.swing.JTextField();
        addData = new javax.swing.JButton();
        editData = new javax.swing.JButton();
        deleteData = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableData = new javax.swing.JTable();
        label_title2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        label_title1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lbl_nama_user = new javax.swing.JLabel();
        btn_logout = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(153, 255, 173));

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel2.setText("Routine Income");

        jLabel12.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel12.setText("Start Date         :");

        startDate.setDateFormatString("yyyy-MMM-dd");

        jLabel11.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel11.setText("End Date        :");

        endDate.setDateFormatString("yyyy-MMM-dd");

        jLabel9.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel9.setText("Description         :");

        jLabel10.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel10.setText("Total Income     :");

        addData.setBackground(new java.awt.Color(1, 169, 182));
        addData.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        addData.setText("Add");
        addData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                addDataMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                addDataMouseExited(evt);
            }
        });
        addData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addDataActionPerformed(evt);
            }
        });

        editData.setBackground(new java.awt.Color(1, 169, 182));
        editData.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        editData.setText("Edit");
        editData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                editDataMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                editDataMouseExited(evt);
            }
        });
        editData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editDataActionPerformed(evt);
            }
        });

        deleteData.setBackground(new java.awt.Color(1, 169, 182));
        deleteData.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        deleteData.setText("Delete");
        deleteData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                deleteDataMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                deleteDataMouseExited(evt);
            }
        });
        deleteData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteDataActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(153, 255, 173));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Detail Pemasukan Rutin", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N

        tableData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Code No.", "Start Date", "End Date", "Description", "Money"
            }
        ));
        tableData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableDataMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableData);
        tableData.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tableData.getAccessibleContext().setAccessibleName("Routine Income Detail");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 805, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        label_title2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_title2.setText("anagement");
        label_title2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_title2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_title2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_title2MouseExited(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 102, 51));
        jLabel4.setText("M");
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel4MouseExited(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 159, 92));
        jLabel3.setText("M");
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel3MouseExited(evt);
            }
        });

        label_title1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_title1.setText("oney");
        label_title1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_title1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_title1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_title1MouseExited(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel5.setText("Hi, ");

        lbl_nama_user.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N

        btn_logout.setBackground(new java.awt.Color(0, 255, 153));
        btn_logout.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        btn_logout.setText("Logout");
        btn_logout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_logoutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_logoutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_logoutMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_title1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_title2)
                        .addGap(288, 288, 288)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_logout)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(293, 293, 293)
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(routineIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(startDate, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(endDate, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(deleteData, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(addData, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(editData, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(142, 142, 142)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(label_title1)
                                .addComponent(jLabel4)
                                .addComponent(label_title2)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(41, 41, 41)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn_logout)
                                    .addComponent(lbl_nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addGap(27, 27, 27)))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(startDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(endDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(routineIncome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(addData, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(editData, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(deleteData, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(756, 756, 756))
        );

        jPanel2.getAccessibleContext().setAccessibleName("Routine Income Detail");
        jPanel2.getAccessibleContext().setAccessibleDescription("Routine Income Detail");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void label_title1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title1MouseExited
        label_title2.setForeground(Color.black);
        label_title1.setForeground(Color.black);
    }//GEN-LAST:event_label_title1MouseExited

    private void label_title1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title1MouseEntered
        label_title2.setForeground(Color.white);
        label_title1.setForeground(Color.white);
    }//GEN-LAST:event_label_title1MouseEntered

    private void label_title1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title1MouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_label_title1MouseClicked

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jLabel3MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jLabel4MouseClicked

    private void label_title2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title2MouseExited
        label_title2.setForeground(Color.black);
        label_title1.setForeground(Color.black);
    }//GEN-LAST:event_label_title2MouseExited

    private void label_title2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title2MouseEntered
        label_title2.setForeground(Color.white);
        label_title1.setForeground(Color.white);
    }//GEN-LAST:event_label_title2MouseEntered

    private void label_title2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title2MouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_label_title2MouseClicked

    private void deleteDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteDataActionPerformed
        try{
            String desc = description.getText();
            String start =((JTextField)startDate.getDateEditor().getUiComponent()).getText();
            String end = ((JTextField)endDate.getDateEditor().getUiComponent()).getText();
            int money =  Integer.parseInt(routineIncome.getText());
            
            routineCashInController datadeleted = new routineCashInController(desc, money, start, end);
            
            datadeleted.delete(idData);
            
            JOptionPane.showMessageDialog(null,"DELETED!");
            
            show();
        }catch(Exception e){
             System.out.println(e);         
        }
    }//GEN-LAST:event_deleteDataActionPerformed

    private void deleteDataMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteDataMouseExited
        deleteData.setForeground(Color.black);
    }//GEN-LAST:event_deleteDataMouseExited

    private void deleteDataMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteDataMouseEntered
        deleteData.setForeground(Color.white);
    }//GEN-LAST:event_deleteDataMouseEntered

    private void editDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editDataActionPerformed
        try{
            String desc = description.getText();
            String start =((JTextField)startDate.getDateEditor().getUiComponent()).getText();
            String end = ((JTextField)endDate.getDateEditor().getUiComponent()).getText();
            int money =  Integer.parseInt(routineIncome.getText());
            
            routineCashInController dataEdited = new routineCashInController(desc, money, start, end);
            
            dataEdited.edit(idData);
               
            JOptionPane.showMessageDialog(null,"EDITED!");
            
            show();
        }catch(Exception e){
            System.out.println(e);         
        }
    }//GEN-LAST:event_editDataActionPerformed

    private void editDataMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editDataMouseExited
        editData.setForeground(Color.black);
    }//GEN-LAST:event_editDataMouseExited

    private void editDataMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editDataMouseEntered
        editData.setForeground(Color.white);
    }//GEN-LAST:event_editDataMouseEntered

    private void addDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addDataActionPerformed
        boolean total = false;
        DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
        Date now = new Date();        
        String startString = df1.format(now);
        
        try {
            now = df1.parse(startString);
        }catch (ParseException ex) {}
        
        if(((JTextField)startDate.getDateEditor().getUiComponent()).getText().equals("") && ((JTextField)endDate.getDateEditor().getUiComponent()).getText().equals("") && description.getText().equals("") && routineIncome.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Pastikan semua telah terisi !");

        }
        else if(((JTextField)startDate.getDateEditor().getUiComponent()).getText().equals("")){
            JOptionPane.showMessageDialog(null, "Start date can not empty!");
        }
        
        else if(((JTextField)endDate.getDateEditor().getUiComponent()).getText().equals("")){
            JOptionPane.showMessageDialog(null, "End date can not empty!");
        }
        
        else if(description.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Description can not empty!");
        }
        
        else if(routineIncome.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Total Income can not empty!");
        }
        
        else if(-1 == startDate.getDate().compareTo(now) || -1 == endDate.getDate().compareTo(now)){
            JOptionPane.showMessageDialog(null, "Start date or End date can not be smaller or same with the current date");
        }
        else if( 1 == startDate.getDate().compareTo(endDate.getDate())){
            JOptionPane.showMessageDialog(null, "End date can not be smaller with the Start date");
        }
        else{
            try{
                String desc = description.getText();
                String start =((JTextField)startDate.getDateEditor().getUiComponent()).getText();
                String end = ((JTextField)endDate.getDateEditor().getUiComponent()).getText();
                int money =  Integer.parseInt(routineIncome.getText());
                
                routineCashInController databaru = new routineCashInController(desc,money, start, end);
                
                databaru.inserttoDb();

                description.setText("");
                ((JTextField)startDate.getDateEditor().getUiComponent()).setText("");
                ((JTextField)endDate.getDateEditor().getUiComponent()).setText("");
                
                routineIncome.setText("");
                
                show();
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, "Data lost");
            }
        }
    }//GEN-LAST:event_addDataActionPerformed

    private void addDataMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addDataMouseExited
        addData.setForeground(Color.black);
    }//GEN-LAST:event_addDataMouseExited

    private void addDataMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addDataMouseEntered
        addData.setForeground(Color.white);
    }//GEN-LAST:event_addDataMouseEntered

    private void jLabel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseEntered
        label_title2.setForeground(Color.white);
        label_title1.setForeground(Color.white);
    }//GEN-LAST:event_jLabel3MouseEntered

    private void jLabel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseEntered
        label_title2.setForeground(Color.white);
        label_title1.setForeground(Color.white);
    }//GEN-LAST:event_jLabel4MouseEntered

    private void jLabel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseExited
        label_title2.setForeground(Color.black);
        label_title1.setForeground(Color.black);
    }//GEN-LAST:event_jLabel3MouseExited

    private void jLabel4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseExited
        label_title2.setForeground(Color.black);
        label_title1.setForeground(Color.black);
    }//GEN-LAST:event_jLabel4MouseExited
    public String namaUser(){
     String dataUser = null;
        try(Connection conn = connect.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            conn.close();
        }catch(Exception e){
            System.out.println(e+"nmuser view ggl");
        }
        return dataUser;
 }
    private void tableDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableDataMouseClicked
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel)tableData.getModel();
        int row = tableData.getSelectedRow();
       
        ((JTextField)startDate.getDateEditor().getUiComponent()).setText(model.getValueAt(row,0).toString());
        ((JTextField)endDate.getDateEditor().getUiComponent()).setText(model.getValueAt(row,1).toString());
        description.setText(model.getValueAt(row,2).toString());
        routineIncome.setText(model.getValueAt(row,3).toString());
        
        String deskripsi = description.getText();
        String start =((JTextField)startDate.getDateEditor().getUiComponent()).getText();
        String end = ((JTextField)endDate.getDateEditor().getUiComponent()).getText();
        int money =  Integer.parseInt(routineIncome.getText());
       
       String user = namaUser();
        try(Connection conn = connect.connect()){ 
                ResultSet IDUSER = conn.createStatement().executeQuery("SELECT id FROM incomeRutin WHERE startDate = '"+start+"' and endDate = '"+end+"' and description = '"+deskripsi+"'  and money = '"+money+"' and username = '"+user+"'");
                while( IDUSER.next()){
                    idData = IDUSER.getInt("id");
                }   
        }catch(Exception e){
          System.out.println(e+"id view gagal ");
        }
 
    }//GEN-LAST:event_tableDataMouseClicked

    private void btn_logoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_logoutMouseClicked
        routineCashInController deleteuser = new routineCashInController();
        
        try{
            deleteuser.deleteUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "can not logout");
        }
        
        new loginView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btn_logoutMouseClicked

    private void btn_logoutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_logoutMouseEntered
        btn_logout.setForeground(Color.blue);
    }//GEN-LAST:event_btn_logoutMouseEntered

    private void btn_logoutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_logoutMouseExited
        btn_logout.setForeground(Color.black);
    }//GEN-LAST:event_btn_logoutMouseExited
    
    public void show() {
        int row = tableData.getRowCount();
        for(int s = 0; s<row;s++){
            tabel.removeRow(0);
        }
        routineCashInController userActiv = new routineCashInController();
        String user = userActiv.userName();
        try{
            Connection conn = connect.connect();
            ResultSet rs = conn.createStatement().executeQuery("select startDate, endDate, description, money from incomeRutin where username= '"+user+"'");
            while(rs.next()){
                String[] data ={rs.getString(1),rs.getString(2),rs.getString(3), rs.getString(4)};
                tabel.addRow(data);
            }
            rs.close();
            conn.close();
            }catch(SQLException ex){} 
    }
   
    public static void main(String args[]) {
       java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new routineCashInView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addData;
    private javax.swing.JLabel btn_logout;
    private javax.swing.JButton deleteData;
    private java.awt.TextArea description;
    private javax.swing.JButton editData;
    private com.toedter.calendar.JDateChooser endDate;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel label_title1;
    private javax.swing.JLabel label_title2;
    private javax.swing.JLabel lbl_nama_user;
    private javax.swing.JTextField routineIncome;
    private com.toedter.calendar.JDateChooser startDate;
    private javax.swing.JTable tableData;
    // End of variables declaration//GEN-END:variables
}
