/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashOut;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import moneymanagement.MoneyManagement;

/**
 *
 * @author Ofri Cantika Valent
 */
public class plannedCashOutModel {
    MoneyManagement connect = new MoneyManagement();
    private String dateBuy;
    private String cashOutName;
    private int charges;
    
    
    public plannedCashOutModel(String des, int money, String date){
        this.cashOutName = des;
        this.charges = money;
        this.dateBuy = date;
    }

    plannedCashOutModel() {}
    
    public String userName(){
    String dataUser = null;
        try(Connection conn = connect.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            conn.close();        
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return dataUser;
 }

    public void deleteSessionUser(){
        String userData = userName() ;
        System.out.println(userData);
        String sql = "DELETE FROM sessionUser WHERE username = '"+userData+"'";
           
        try(Connection conn = connect.connect(); PreparedStatement st = conn.prepareStatement(sql)){
            st.executeUpdate();          
            conn.close();
        }catch(Exception e){} 
    }
    
    public void insert(){
        String userData = userName();
        
        try(Connection conn = connect.connect()){
            String sqlQuery = "INSERT INTO unplannedCashOut(dateBuying, cashOutName, charge, username) values(?,?,?,?)";
            
            try( PreparedStatement kalimat = conn.prepareStatement(sqlQuery)){              
                kalimat.setString(1, this.dateBuy);
                kalimat.setString(2, this.cashOutName);
                kalimat.setInt(3, this.charges);
                kalimat.setString(4, userData);
                kalimat.executeUpdate();

                JOptionPane.showMessageDialog(null, "Data is saved"); 
            } 
            conn.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
    public void edit(int id){
        String nama_user = userName();
        String sql = "UPDATE unplannedCashOut SET dateBuying = ?, cashOutName = ? , charge = ?   WHERE id = ? ";
        System.out.println(id+this.charges+this.cashOutName+this.dateBuy); 
        try(Connection conn = connect.connect()){
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, this.dateBuy);
            st.setString(2, this.cashOutName);
            st.setInt(3, this.charges);
            st.setInt(4, id);   
            st.executeUpdate();
        
            System.out.println(id+this.charges+this.cashOutName+this.dateBuy);
            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    public void delete(int id){
        String sql = "DELETE FROM unplannedCashOut WHERE id = '"+id+"'";
        
        try(Connection conn = connect.connect(); PreparedStatement st = conn.prepareStatement(sql)){
            st.executeUpdate();
        }catch(Exception e){
            System.out.println(e+"delete model ggl");
        }
    }
}
