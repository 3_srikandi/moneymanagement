
package cashOut;

import Login.loginView;
import cashIn.routineCashInController;
import homePage.homePageView;
import java.awt.Color;
import cashOut.plannedCashOutController;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import moneymanagement.MoneyManagement;

public class plannedCashOutView extends javax.swing.JFrame {
    private DefaultTableModel tabel;
    MoneyManagement koneksi = new MoneyManagement();
    int idData;
    int total = 0;
    public plannedCashOutView() {
        initComponents();
        String name = userName();
        nama_user.setText(name);
        JLabel label = new JLabel("hello");
        String[] title = {"Date_Buying","Cash-Out_Name","Charge"};
        tabel = new DefaultTableModel(title, 0);
        dataTable.setModel(tabel);
        show();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_back = new javax.swing.JLabel();
        btn_back1 = new javax.swing.JLabel();
        btn_back2 = new javax.swing.JLabel();
        btn_back3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btn_logout = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cashOutName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        charge = new javax.swing.JTextField();
        addData = new javax.swing.JButton();
        editData = new javax.swing.JButton();
        deleteData = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        dateBuying = new com.toedter.calendar.JDateChooser();
        nama_user = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        show_total = new javax.swing.JLabel();
        label_try = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        dataTable = new javax.swing.JTable();
        srch_data = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        logout = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        btn_back.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        btn_back.setForeground(new java.awt.Color(0, 159, 92));
        btn_back.setText("M");
        btn_back.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_backMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_backMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_backMouseExited(evt);
            }
        });

        btn_back1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btn_back1.setText("oney");
        btn_back1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_back1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_back1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_back1MouseExited(evt);
            }
        });

        btn_back2.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        btn_back2.setForeground(new java.awt.Color(255, 102, 51));
        btn_back2.setText("M");
        btn_back2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_back2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_back2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_back2MouseExited(evt);
            }
        });

        btn_back3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btn_back3.setText("anagement");
        btn_back3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_back3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_back3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_back3MouseExited(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel2.setText("Pengeluaran Tidak Direncanakan");

        btn_logout.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        btn_logout.setText("   Logout");
        btn_logout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_logoutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_logoutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_logoutMouseExited(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel1.setText("Cash-Out Name  :");

        cashOutName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cashOutNameActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel5.setText("Charge             :");

        addData.setBackground(new java.awt.Color(1, 169, 182));
        addData.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        addData.setText("Add");
        addData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                addDataMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                addDataMouseExited(evt);
            }
        });
        addData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addDataActionPerformed(evt);
            }
        });

        editData.setBackground(new java.awt.Color(1, 169, 182));
        editData.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        editData.setText("Edit");
        editData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                editDataMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                editDataMouseExited(evt);
            }
        });
        editData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editDataActionPerformed(evt);
            }
        });

        deleteData.setBackground(new java.awt.Color(1, 169, 182));
        deleteData.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        deleteData.setText("Delete");
        deleteData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                deleteDataMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                deleteDataMouseExited(evt);
            }
        });
        deleteData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteDataActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel12.setText("Date Buying       :");

        dateBuying.setDateFormatString("yyyy-MMM-dd");

        nama_user.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel3.setText("Hi,");

        jLabel6.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel6.setText("Detail Pengeluaran Tidak Direncanakan");

        show_total.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        label_try.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        label_try.setText("Total Pengeluaran Tidak Direncanakan:   Rp ");

        dataTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No Code", "Date Buying", "Cash-Out Name", "Charge"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        dataTable.setColumnSelectionAllowed(true);
        dataTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dataTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(dataTable);
        dataTable.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        srch_data.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        srch_data.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                srch_dataKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel4.setText("Search :");

        logout.setBackground(new java.awt.Color(0, 255, 153));
        logout.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        logout.setText("Logout");
        logout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoutMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_back, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cashOutName, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel5)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(charge)))
                                        .addGap(198, 198, 198))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel12)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(dateBuying, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(deleteData, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
                                    .addComponent(editData, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(addData, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(323, 323, 323))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btn_back1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_back2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_back3)
                                .addGap(284, 284, 284)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(logout)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addComponent(btn_logout, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(285, 285, 285))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(182, 182, 182)
                        .addComponent(jLabel2)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 719, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(label_try)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(show_total, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(srch_data, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(701, 701, 701))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(271, 271, 271)
                .addComponent(jLabel6)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btn_logout, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(logout))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1))
                            .addComponent(nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_back, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_back1)
                            .addComponent(btn_back2)
                            .addComponent(btn_back3))))
                .addGap(8, 8, 8)
                .addComponent(jLabel2)
                .addGap(46, 46, 46)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addData, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(dateBuying, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(cashOutName, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(charge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(editData, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(deleteData, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(44, 44, 44)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(srch_data, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(show_total, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label_try, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)))
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(273, 273, 273))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 779, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 681, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_backMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_backMouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btn_backMouseClicked

    private void btn_backMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_backMouseEntered
        btn_back3.setForeground(Color.white);
        btn_back1.setForeground(Color.white);
    }//GEN-LAST:event_btn_backMouseEntered

    private void btn_backMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_backMouseExited
        btn_back3.setForeground(Color.black);
        btn_back1.setForeground(Color.black);
    }//GEN-LAST:event_btn_backMouseExited

    private void btn_back1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_back1MouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btn_back1MouseClicked

    private void btn_back1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_back1MouseEntered
        btn_back3.setForeground(Color.white);
        btn_back1.setForeground(Color.white);
    }//GEN-LAST:event_btn_back1MouseEntered

    private void btn_back1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_back1MouseExited
        btn_back3.setForeground(Color.black);
        btn_back1.setForeground(Color.black);
    }//GEN-LAST:event_btn_back1MouseExited

    private void btn_back2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_back2MouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btn_back2MouseClicked

    private void btn_back2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_back2MouseEntered
        btn_back3.setForeground(Color.white);
        btn_back1.setForeground(Color.white);
    }//GEN-LAST:event_btn_back2MouseEntered

    private void btn_back2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_back2MouseExited
        btn_back3.setForeground(Color.black);
        btn_back1.setForeground(Color.black);
    }//GEN-LAST:event_btn_back2MouseExited

    private void btn_back3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_back3MouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btn_back3MouseClicked

    private void btn_back3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_back3MouseEntered
        btn_back3.setForeground(Color.white);
        btn_back1.setForeground(Color.white);
    }//GEN-LAST:event_btn_back3MouseEntered

    private void btn_back3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_back3MouseExited
        btn_back3.setForeground(Color.black);
        btn_back1.setForeground(Color.black);
    }//GEN-LAST:event_btn_back3MouseExited

    private void cashOutNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cashOutNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cashOutNameActionPerformed

    private void addDataMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addDataMouseEntered
        addData.setForeground(Color.white);
    }//GEN-LAST:event_addDataMouseEntered

    private void addDataMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addDataMouseExited
        addData.setForeground(Color.black);
    }//GEN-LAST:event_addDataMouseExited

    private void addDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addDataActionPerformed
        // TODO add your handling code here:
        boolean total = false;
        DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
        Date now = new Date();
        
        
        String startString = df1.format(now);
        try {
            now = df1.parse(startString);
        }catch (ParseException ex) {
            
        }

        if(((JTextField)dateBuying.getDateEditor().getUiComponent()).getText().equals("") && cashOutName.getText().equals("") && charge.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Pastikan semua telah terisi !");

        }
        else if(((JTextField)dateBuying.getDateEditor().getUiComponent()).getText().equals("")){
            JOptionPane.showMessageDialog(null, "Start date can not empty!");
        }
        else if(cashOutName.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Description can not empty!");
        }
        else if(charge.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Total Income can not empty!");
        }
//        else if(-1 == dateBuying.getDate().compareTo(sekarang)){
//            JOptionPane.showMessageDialog(null, "datecan not be smaller or same with the current date");
//        }
       
        else{
            try{
                String cashoutName = cashOutName.getText();
                String dateBuy =((JTextField)dateBuying.getDateEditor().getUiComponent()).getText();
                int charges =  Integer.parseInt(charge.getText());
                plannedCashOutController databaru = new plannedCashOutController(cashoutName, charges, dateBuy);
        
                databaru.insertData();

                cashOutName.setText("");
                ((JTextField)dateBuying.getDateEditor().getUiComponent()).setText("");
                charge.setText("");
                
                show();
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, "Data lost");
            }
        }
        
    }//GEN-LAST:event_addDataActionPerformed

    private void editDataMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editDataMouseEntered
        editData.setForeground(Color.white);
    }//GEN-LAST:event_editDataMouseEntered

    private void editDataMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editDataMouseExited
        editData.setForeground(Color.black);
    }//GEN-LAST:event_editDataMouseExited

    private void editDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editDataActionPerformed
        try{
            String cashoutName = cashOutName.getText(); 
            String dateBuy =((JTextField)dateBuying.getDateEditor().getUiComponent()).getText();
            int charges =  Integer.parseInt(charge.getText());
            plannedCashOutController dataEdited = new plannedCashOutController(cashoutName, charges, dateBuy);
            dataEdited.edit(idData);
            System.out.println(idData+"diview");
            JOptionPane.showMessageDialog(null,"EDITED!");
            show();
        }catch(Exception e){
             System.out.println(e.getMessage());         
        }
    }//GEN-LAST:event_editDataActionPerformed

    private void deleteDataMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteDataMouseEntered
        deleteData.setForeground(Color.white);
    }//GEN-LAST:event_deleteDataMouseEntered

    private void deleteDataMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteDataMouseExited
        deleteData.setForeground(Color.black);
    }//GEN-LAST:event_deleteDataMouseExited

    private void deleteDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteDataActionPerformed
         try{
            
            String cashoutName = cashOutName.getText();
            String dateBuy =((JTextField)dateBuying.getDateEditor().getUiComponent()).getText();
            int charges =  Integer.parseInt(charge.getText());   
            plannedCashOutController del_data = new plannedCashOutController(cashoutName, charges, dateBuy);
            
            del_data.delete(idData);
            System.out.println(idData);
            
            JOptionPane.showMessageDialog(null,"DELETED!");
            
            show();
        }catch(Exception e){
             System.out.println(e);         
        }
    }//GEN-LAST:event_deleteDataActionPerformed
 
    private void dataTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dataTableMouseClicked
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel)dataTable.getModel();
        int row = dataTable.getSelectedRow();
       
        ((JTextField)dateBuying.getDateEditor().getUiComponent()).setText(model.getValueAt(row,0).toString());
        cashOutName.setText(model.getValueAt(row,1).toString());
        charge.setText(model.getValueAt(row,2).toString());
        
        String dsc = cashOutName.getText();
        String date =((JTextField)dateBuying.getDateEditor().getUiComponent()).getText();
        int money =  Integer.parseInt(charge.getText());
       
       //PengeluaranTidakDirencanakanController getUsername = new PengeluaranTidakDirencanakanController();
        String user = userName();
        try(Connection conn = koneksi.connect()){ 
                ResultSet IDUSER = conn.createStatement().executeQuery("SELECT id FROM unplannedCashOut WHERE dateBuying = '"+date+"' and cashOutName = '"+dsc+"'  and charge = '"+money+"' and username = '"+user+"'");
                while( IDUSER.next()){
                    idData = IDUSER.getInt("id");
                }   
        }catch(Exception e){
          System.out.println(e+"id view gagal ");
        }
    }//GEN-LAST:event_dataTableMouseClicked

    private void logoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseClicked
        plannedCashOutController deleteuser = new plannedCashOutController();
        try{
            deleteuser.deleteSessUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "can not logout");
        }
        new loginView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoutMouseClicked

    private void logoutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseEntered
        logout.setForeground(Color.blue);
    }//GEN-LAST:event_logoutMouseEntered

    private void logoutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseExited
        logout.setForeground(Color.black);
    }//GEN-LAST:event_logoutMouseExited

    private void btn_logoutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_logoutMouseExited
        btn_logout.setForeground(Color.black);
    }//GEN-LAST:event_btn_logoutMouseExited

    private void btn_logoutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_logoutMouseEntered
        btn_logout.setForeground(Color.blue);
    }//GEN-LAST:event_btn_logoutMouseEntered

    private void btn_logoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_logoutMouseClicked
        plannedCashOutController delSession = new  plannedCashOutController();
        try{
            delSession.deleteSessUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "can not logout");
        }
        new loginView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btn_logoutMouseClicked

    private void srch_dataKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_srch_dataKeyReleased
        // TODO add your handling code here:
        String query = srch_data.getText().toLowerCase();
        filter(query);
    }//GEN-LAST:event_srch_dataKeyReleased
    
    public String userName(){
     String userData = null;
        try(Connection conn = koneksi.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            
            username.next();
            
            userData = username.getString("username");
            
            conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser view ggl");
        }
        return userData;
    }
    
    public void show() {
        total = 0;
        int row = dataTable.getRowCount();
        for(int s = 0; s<row; s++){
            tabel.removeRow(0);
        }
        //PengeluaranTidakDirencanakanModel userActiv = new PengeluaranTidakDirencanakanModel();
        String user = userName();
        try{
            System.out.println(user);
            Connection conn = koneksi.connect();
            ResultSet rs = conn.createStatement().executeQuery("select dateBuying, cashOutName, charge from unplannedCashOut where username= '"+user+"'");
                
            while(rs.next()){
                total = total + rs.getInt(3);
                String[] data ={rs.getString(1),rs.getString(2),rs.getString(3)};
                tabel.addRow(data);
            }
            
            String shw_total = Integer.toString(total);
            show_total.setText(shw_total);
            rs.close();
            conn.close();
            
        }catch(SQLException ex){}
    }
  
    public void filter(String query){
            TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(tabel);
            dataTable.setRowSorter(tr);
            
            tr.setRowFilter(RowFilter.regexFilter(query));
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(plannedCashOutView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(plannedCashOutView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(plannedCashOutView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(plannedCashOutView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new plannedCashOutView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addData;
    private javax.swing.JLabel btn_back;
    private javax.swing.JLabel btn_back1;
    private javax.swing.JLabel btn_back2;
    private javax.swing.JLabel btn_back3;
    private javax.swing.JLabel btn_logout;
    private javax.swing.JTextField cashOutName;
    private javax.swing.JTextField charge;
    private javax.swing.JTable dataTable;
    private com.toedter.calendar.JDateChooser dateBuying;
    private javax.swing.JButton deleteData;
    private javax.swing.JButton editData;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel label_try;
    private javax.swing.JLabel logout;
    private javax.swing.JLabel nama_user;
    private javax.swing.JLabel show_total;
    private javax.swing.JTextField srch_data;
    // End of variables declaration//GEN-END:variables
}
