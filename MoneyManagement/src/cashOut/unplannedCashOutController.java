/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashOut;

import javax.swing.JOptionPane;

/**
 *
 * @author Cyndy Alisia
 */
public class unplannedCashOutController {
    private String deadlineDate;
    private String cashOutName;
    private int charge;
    private unplannedCashOutModel1 newData;
    
   
    public unplannedCashOutController(String date, String name, int charge){
        this.deadlineDate = date;
        this.cashOutName = name;
        this.charge = charge;
        newData = new unplannedCashOutModel1(date, name, charge);
    }

    public unplannedCashOutController(){}
    
    public void inserttoDb(){
         try{  
            newData.insert();
                          
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, "Can not to access!");
            }
    }
    
    public void deleteUser(){
        unplannedCashOutModel1 sendUser = new unplannedCashOutModel1();
        try{
            sendUser.deleteSessionUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
     
    public String userName(){
        unplannedCashOutModel1 sendUser = new unplannedCashOutModel1();
        try {
            String nama = sendUser.userName();
            return nama;
        }catch(Exception e){
             
        }
        return "";
     }
     
    public void delete(int idData){
        try{
            newData.deleteData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
     }
     
    public void edit(int idData){
        try{
            newData.editData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to edit controller");
        }
     }

    
}
