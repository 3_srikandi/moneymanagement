/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashOut;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import moneymanagement.MoneyManagement;

/**
 *
 * @author Cyndy Alisia
 */
public class unplannedCashOutModel1 {
    private String deadlineDate;
    private String cashOutName;
    private int charge;
    private String user;
    private int iddata;
    ResultSet rs = null;
    MoneyManagement connect = new MoneyManagement();
    
    public unplannedCashOutModel1(){}
    
    public unplannedCashOutModel1(String date, String name, int charge){
        this.deadlineDate = date;
        this.cashOutName = name;
        this.charge = charge;
    }

    public String userName(){
        String dataUser = null;
        try(Connection conn = connect.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser ggl");
        }
        return dataUser;
    }
    
    public void insert(){
        String userData = userName();
        String sqlQuery = "INSERT INTO planCashOut(deadlineDate, cashOutName, charge, username) values(?,?,?,?)";
        try(Connection conn = connect.connect();
            PreparedStatement kalimat = conn.prepareStatement(sqlQuery)){              
                kalimat.setString(1, this.deadlineDate);
                kalimat.setString(2, this.cashOutName);
                kalimat.setInt(3, this.charge);
                kalimat.setString(4, userData);
                kalimat.executeUpdate();
                JOptionPane.showMessageDialog(null, "Data is saved"); 
                
            conn.close();
        }catch(Exception e){
            //JOptionPane.showMessageDialog(null, "can not select model ");
            System.out.println(e.getMessage());
            
        } 
    }
  
    public void deleteSessionUser() {
    String userData = userName() ;
    try(Connection conn = connect.connect()){        
        String sql = "DELETE FROM sessionUser WHERE username = '"+userData+"'";
           
        try(PreparedStatement st = conn.prepareStatement(sql)){

            st.executeUpdate();
//                JOptionPane.showMessageDialog(null, "can delete model");
//                st.close();
        }catch(Exception e){
//                JOptionPane.showMessageDialog(null, "can not delete model");
        }
        conn.close();
        }catch(Exception e){
//                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
 
  
    public void deleteData(int idData) {
        //int id = idDataDelete();
        System.out.println(idData+deadlineDate+cashOutName+charge);
        String sql = "DELETE FROM planCashOut WHERE id = '"+idData+"'";
        
        try(Connection conn = connect.connect(); PreparedStatement st = conn.prepareStatement(sql)){
        st.executeUpdate();
            
//                conn.close();
        }catch(Exception e){
            System.out.println(e+"delete model ggl");
        }
     
    }
    public void editData(int idData) {
        String user_name = userName();
        System.out.println(idData+deadlineDate+cashOutName+charge);
        String sql = "UPDATE planCashOut SET deadlineDate = ?, cashOutName = ? , charge = ?  WHERE id = ? ";
          
        try(Connection conn = connect.connect()){
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, this.deadlineDate);
            st.setString(2, this.cashOutName);
            st.setInt(3, this.charge);
            st.setInt(4, idData);
            st.executeUpdate();
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
    }
 
}