
package cashOut;

public class plannedCashOutController {
    private String deadlineDate, cashOutName;
    private int charge;
    private plannedCashOutModel newData;
    
    
    public plannedCashOutController() {}
    public plannedCashOutController(String des, int money, String date){
        this.cashOutName = des;
        this.charge = money;
        this.deadlineDate = date;
        
        newData = new plannedCashOutModel(des,money,date);
    }

    public String userName(){
        String user = null;
        try{
            user = newData.userName();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return user;
    }
   
    
    public void deleteSessUser(){
        try{
            newData.deleteSessionUser();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void edit(int id){
         try{
            newData.edit(id);
        }catch(Exception e){
            System.out.println(e.getMessage()+"controller"+"..."+id);
        }
    }
    public void insertData(){
         try{
            newData.insert();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void delete(int id){
         try{
            newData.delete(id);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
