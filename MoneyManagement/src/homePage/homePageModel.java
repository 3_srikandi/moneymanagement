
package homePage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import moneymanagement.MoneyManagement;



public class homePageModel {
    MoneyManagement connect = new MoneyManagement();
    public String userData;
    public void deleteSessionUser() throws ClassNotFoundException, SQLException{
        try(Connection conn = connect.connect()){
            ResultSet username = conn.createStatement().executeQuery("select username from sessionUser");
            username.next();
            userData = username.getString("username");
            System.out.println(userData);
            String sql = "DELETE FROM sessionUser WHERE username = '"+userData+"'";

            try(PreparedStatement st = conn.prepareStatement(sql)){          
                st.executeUpdate();
                username.close();
                st.close();
            }catch(Exception e){}
             conn.close();
        }catch(Exception e){}
    }
}