
package homePage;

import java.awt.Color;
import javax.swing.JOptionPane;
import Login.loginView;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import moneymanagement.MoneyManagement;
import Deadline.deadlineCashOutView;
public class homePageView extends javax.swing.JFrame {
    MoneyManagement koneksi = new MoneyManagement();
    
    public homePageView() {
        initComponents();
        
        String nama = userName();
        lbl_nama_user.setText(nama);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        btn_pemasukan = new javax.swing.JButton();
        btn_pengeluaran = new javax.swing.JButton();
        btn_repeated = new javax.swing.JButton();
        btn_deadline = new javax.swing.JButton();
        btn_laporan = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbl_nama_user = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        logout = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(153, 255, 173));
        jPanel3.setForeground(new java.awt.Color(204, 255, 204));

        jLabel3.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 159, 92));
        jLabel3.setText("M");

        btn_pemasukan.setBackground(new java.awt.Color(1, 169, 182));
        btn_pemasukan.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btn_pemasukan.setText("Cash-In");
        btn_pemasukan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_pemasukanMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_pemasukanMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_pemasukanMouseExited(evt);
            }
        });
        btn_pemasukan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_pemasukanActionPerformed(evt);
            }
        });

        btn_pengeluaran.setBackground(new java.awt.Color(1, 169, 182));
        btn_pengeluaran.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btn_pengeluaran.setText("Cash-Out");
        btn_pengeluaran.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_pengeluaranMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_pengeluaranMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_pengeluaranMouseExited(evt);
            }
        });

        btn_repeated.setBackground(new java.awt.Color(1, 169, 182));
        btn_repeated.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btn_repeated.setText("Repeated Cash-In");
        btn_repeated.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_repeatedMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_repeatedMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_repeatedMouseExited(evt);
            }
        });

        btn_deadline.setBackground(new java.awt.Color(1, 169, 182));
        btn_deadline.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btn_deadline.setText("Deadline Cash-Out");
        btn_deadline.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_deadlineMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_deadlineMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_deadlineMouseExited(evt);
            }
        });

        btn_laporan.setBackground(new java.awt.Color(1, 169, 182));
        btn_laporan.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btn_laporan.setText("Report");
        btn_laporan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_laporanMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_laporanMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_laporanMouseExited(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 102, 51));
        jLabel4.setText("M");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(102, 102, 102));
        jLabel6.setText("anagement");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(102, 102, 102));
        jLabel7.setText("oney");

        lbl_nama_user.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel5.setText("Hi, ");

        logout.setBackground(new java.awt.Color(0, 255, 153));
        logout.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        logout.setText("Logout");
        logout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoutMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addGap(2, 2, 2)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(btn_pemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(171, Short.MAX_VALUE))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btn_repeated, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btn_pengeluaran, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btn_deadline, javax.swing.GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE))
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 92, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addComponent(logout)
                        .addGap(19, 19, 19))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel7)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(logout)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(lbl_nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                        .addComponent(btn_pemasukan)
                        .addGap(18, 18, 18)
                        .addComponent(btn_pengeluaran)
                        .addGap(18, 18, 18)
                        .addComponent(btn_repeated)
                        .addGap(18, 18, 18)
                        .addComponent(btn_deadline)
                        .addGap(18, 18, 18)
                        .addComponent(btn_laporan)))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public String userName(){
     String userData = null;
        try(Connection conn = koneksi.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            userData = username.getString("username");
            
//            username.close();
           conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser view ggl");
        }
        return userData;
 }
    private void btn_pemasukanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_pemasukanMouseClicked

        //JOptionPane.showMessageDialog(null, "PEMASUKAN");
        new subMenuPemasukan().setVisible(true); //untuk menampilkan form baru
        this.setVisible(false);
    }//GEN-LAST:event_btn_pemasukanMouseClicked

    private void btn_pemasukanMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_pemasukanMouseEntered
        btn_pemasukan.setForeground(Color.white);
    }//GEN-LAST:event_btn_pemasukanMouseEntered

    private void btn_pemasukanMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_pemasukanMouseExited
        btn_pemasukan.setForeground(Color.black);
    }//GEN-LAST:event_btn_pemasukanMouseExited

    private void btn_pemasukanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_pemasukanActionPerformed
        
    }//GEN-LAST:event_btn_pemasukanActionPerformed

    private void btn_pengeluaranMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_pengeluaranMouseClicked
         new subMenuPengeluaran().setVisible(true); //untuk menampilkan form baru
        this.setVisible(false);
    }//GEN-LAST:event_btn_pengeluaranMouseClicked

    private void btn_pengeluaranMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_pengeluaranMouseEntered
        btn_pengeluaran.setForeground(Color.white);
    }//GEN-LAST:event_btn_pengeluaranMouseEntered

    private void btn_pengeluaranMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_pengeluaranMouseExited
        btn_pengeluaran.setForeground(Color.black);
    }//GEN-LAST:event_btn_pengeluaranMouseExited

    private void btn_repeatedMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_repeatedMouseClicked
        JOptionPane.showMessageDialog(null, "REPEATED");
        //new loginView().setVisible(true); //untuk menampilkan form baru
        //this.setVisible(false);
    }//GEN-LAST:event_btn_repeatedMouseClicked

    private void btn_repeatedMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_repeatedMouseEntered
        btn_repeated.setForeground(Color.white);
    }//GEN-LAST:event_btn_repeatedMouseEntered

    private void btn_repeatedMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_repeatedMouseExited
        btn_repeated.setForeground(Color.black);
    }//GEN-LAST:event_btn_repeatedMouseExited

    private void btn_deadlineMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_deadlineMouseClicked
        JOptionPane.showMessageDialog(null, "DEADLINE");
        new deadlineCashOutView().setVisible(true);
        //new loginView().setVisible(true); //untuk menampilkan form baru
        this.setVisible(false);
    }//GEN-LAST:event_btn_deadlineMouseClicked

    private void btn_deadlineMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_deadlineMouseEntered
        btn_deadline.setForeground(Color.white);
    }//GEN-LAST:event_btn_deadlineMouseEntered

    private void btn_deadlineMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_deadlineMouseExited
        btn_deadline.setForeground(Color.black);
    }//GEN-LAST:event_btn_deadlineMouseExited

    private void btn_laporanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_laporanMouseClicked
        JOptionPane.showMessageDialog(null, "REPORT");
        //new loginView().setVisible(true); //untuk menampilkan form baru
        //this.setVisible(false);
    }//GEN-LAST:event_btn_laporanMouseClicked

    private void btn_laporanMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_laporanMouseEntered
        btn_laporan.setForeground(Color.white);
    }//GEN-LAST:event_btn_laporanMouseEntered

    private void btn_laporanMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_laporanMouseExited
        btn_laporan.setForeground(Color.black);
    }//GEN-LAST:event_btn_laporanMouseExited

    private void logoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseClicked
        homePageController deleteUser = new homePageController();
        try{
            deleteUser.deleteUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "can not logout");
        }
        new loginView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoutMouseClicked

    private void logoutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseEntered
        logout.setForeground(Color.blue);
    }//GEN-LAST:event_logoutMouseEntered

    private void logoutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseExited
        logout.setForeground(Color.black);
    }//GEN-LAST:event_logoutMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(homePageView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(homePageView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(homePageView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(homePageView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new homePageView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_deadline;
    private javax.swing.JButton btn_laporan;
    private javax.swing.JButton btn_pemasukan;
    private javax.swing.JButton btn_pengeluaran;
    private javax.swing.JButton btn_repeated;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lbl_nama_user;
    private javax.swing.JLabel logout;
    // End of variables declaration//GEN-END:variables
}
