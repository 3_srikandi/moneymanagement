
package Deadline;

import homePage.homePageView;
import javax.swing.JOptionPane;
import Login.loginView;
import cashOut.unplannedCashOutController;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
import moneymanagement.MoneyManagement;
import moneymanagement.MoneyManagement;

public class deadlineCashOutView extends javax.swing.JFrame {
      private DefaultTableModel tabel;
    MoneyManagement koneksi = new MoneyManagement();
    
    public deadlineCashOutView() {
        initComponents();
        
        String[] judul = {"Deadline_Date","Cash-Out_Name","Charge"};
        tabel = new DefaultTableModel(judul, 0);
        deadlineTable.setModel(tabel);
        tampil();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        deadlineButton = new javax.swing.JLabel();
        logoM = new javax.swing.JLabel();
        logoOney = new javax.swing.JLabel();
        logoM2 = new javax.swing.JLabel();
        logoAnagement = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        deadlineTable = new javax.swing.JTable();
        logoutLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 255, 173));

        deadlineButton.setBackground(new java.awt.Color(153, 255, 173));
        deadlineButton.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        deadlineButton.setText("Deadline Cash-Out");

        logoM.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        logoM.setForeground(new java.awt.Color(0, 159, 92));
        logoM.setText("M");
        logoM.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoMMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoMMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoMMouseExited(evt);
            }
        });

        logoOney.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        logoOney.setForeground(new java.awt.Color(102, 102, 102));
        logoOney.setText("oney");
        logoOney.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoOneyMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoOneyMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoOneyMouseExited(evt);
            }
        });

        logoM2.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        logoM2.setForeground(new java.awt.Color(255, 102, 51));
        logoM2.setText("M");
        logoM2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoM2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoM2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoM2MouseExited(evt);
            }
        });

        logoAnagement.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        logoAnagement.setForeground(new java.awt.Color(102, 102, 102));
        logoAnagement.setText("anagement");
        logoAnagement.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoAnagementMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoAnagementMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoAnagementMouseExited(evt);
            }
        });

        deadlineTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No Code", "Deadline Date", "Cash-Out Name", "Charge"
            }
        ));
        deadlineTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deadlineTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(deadlineTable);
        deadlineTable.getAccessibleContext().setAccessibleName("Deadline Cash-Out Detail");

        logoutLabel.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        logoutLabel.setText("Logout");
        logoutLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoutLabelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoutLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoutLabelMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(logoM)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(logoOney)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(logoM2)
                        .addGap(0, 0, 0)
                        .addComponent(logoAnagement)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(logoutLabel)
                        .addGap(23, 23, 23))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 5, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(deadlineButton)
                                .addGap(131, 131, 131))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(logoM)
                    .addComponent(logoOney)
                    .addComponent(logoM2)
                    .addComponent(logoAnagement)
                    .addComponent(logoutLabel))
                .addGap(40, 40, 40)
                .addComponent(deadlineButton)
                .addGap(34, 34, 34)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(51, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void logoutLabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutLabelMouseClicked
           // TODO add your handling code here:
         deadlineCashOutController deleteuser = new deadlineCashOutController();
//        try{
//             deleteuser.hapusUser();
//        }catch(Exception e){
//            JOptionPane.showMessageDialog(null, "can not delete view");
//        }                             
        new loginView().setVisible(true); //untuk menampilkan form baru
        this.setVisible(false);   
    }//GEN-LAST:event_logoutLabelMouseClicked

    private void logoutLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutLabelMouseEntered
          logoutLabel.setForeground(Color.blue);
    }//GEN-LAST:event_logoutLabelMouseEntered

    private void logoutLabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutLabelMouseExited
         logoutLabel.setForeground(Color.black);
    }//GEN-LAST:event_logoutLabelMouseExited

    private void logoMMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoMMouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoMMouseClicked

    private void logoOneyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoOneyMouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoOneyMouseClicked

    private void logoM2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoM2MouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoM2MouseClicked

    private void logoAnagementMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoAnagementMouseClicked
        new homePageView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoAnagementMouseClicked

    private void logoMMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoMMouseEntered
        logoOney.setForeground(Color.white);
        logoAnagement.setForeground(Color.white);
    }//GEN-LAST:event_logoMMouseEntered

    private void logoMMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoMMouseExited
         logoOney.setForeground(Color.black);
        logoAnagement.setForeground(Color.black);
    }//GEN-LAST:event_logoMMouseExited

    private void logoOneyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoOneyMouseEntered
         logoOney.setForeground(Color.white);
        logoAnagement.setForeground(Color.white);
    }//GEN-LAST:event_logoOneyMouseEntered

    private void logoOneyMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoOneyMouseExited
        logoOney.setForeground(Color.black);
        logoAnagement.setForeground(Color.black);
    }//GEN-LAST:event_logoOneyMouseExited

    private void logoM2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoM2MouseEntered

    }//GEN-LAST:event_logoM2MouseEntered

    private void logoM2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoM2MouseExited

    }//GEN-LAST:event_logoM2MouseExited

    private void logoAnagementMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoAnagementMouseExited
       logoOney.setForeground(Color.black);
        logoAnagement.setForeground(Color.black);
    }//GEN-LAST:event_logoAnagementMouseExited

    private void logoAnagementMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoAnagementMouseEntered
         logoOney.setForeground(Color.white);
        logoAnagement.setForeground(Color.white);
    }//GEN-LAST:event_logoAnagementMouseEntered

    private void deadlineTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deadlineTableMouseClicked
        
    }//GEN-LAST:event_deadlineTableMouseClicked

//   
//     public void tampil(){
//        int row = deadlineTable.getRowCount();
//        DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
//        Date sekarang = new Date();
//        Date date;
//        String tanggal;
//        
//        for(int s = 0; s<row;s++){
//            tabel.removeRow(0);
//        }
//        pengeluarandiRencanakanController userActiv = new pengeluarandiRencanakanController();
//        String user = userActiv.namaUSer();
//        String sqlQuery = "SELECT deadlineDate, cashOutName, charge FROM planCashOut where username = ?";
//            
//        try(Connection conn = koneksi.connect();
//            PreparedStatement ps = conn.prepareStatement(sqlQuery);){
//            
//            ps.setString(1, user);
//            
//            ResultSet rs = ps.executeQuery();
//            
//            while(rs.next()){
//                System.out.println(rs.getString("deadlineDate"));
//                System.out.println(rs.getString("cashOutName"));
//                System.out.println(Integer.toString(rs.getInt("charge")));
//                
//                tanggal = rs.getString("deadlineDate");
//                date = df1.parse(tanggal);
//                int i = (int)date.getTime(); //tgl dari deadline
//                int j = (int)sekarang.getTime(); // tgl sekarang
//                int k = i - j;
//                
//                if (k <= 3 && k != 0){
//                    String[] data ={rs.getString("deadlineDate"),rs.getString("cashOutName"),Integer.toString(rs.getInt("charge"))};
//                    System.out.println(k);
//                    tabel.addRow(data);
//                }
//            }
//            conn.close();
//            
//        }catch(Exception e){
//            //JOptionPane.showMessageDialog(null, "can not select model ");
//            System.out.println(e.getMessage());
//        }
//    }
    public String namaUser(){
     String dataUser = null;
        try(Connection conn = koneksi.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            
//            username.close();
           conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser view ggl");
        }
        return dataUser;
 }
     public void tampil()  {
      
        int row = deadlineTable.getRowCount();
        for(int s = 0; s<row; s++){
            tabel.removeRow(0);
        }
        //PengeluaranTidakDirencanakanModel userActiv = new PengeluaranTidakDirencanakanModel();
        String user = namaUser(); String tanggal;
        try{
            
            Connection conn = koneksi.connect();
            ResultSet rs = conn.createStatement().executeQuery("select deadlineDate, cashOutName, charge from planCashOut where username= '"+user+"'");
            Date date; 
            Calendar cal = Calendar.getInstance();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd");
            String datesekarang = dateFormat.format(cal.getTime());
            System.out.println(datesekarang);
            
            cal.add(Calendar.DATE, 2);
            String deadlineH2 =dateFormat.format(cal.getTime());
            System.out.println("h2: "+deadlineH2);
            
            cal.add(Calendar.DATE, -1);
            String deadlineH1 =dateFormat.format(cal.getTime());
            System.out.println("h1: "+deadlineH1);
            
            cal.add(Calendar.DATE, -1);
            String deadlineH=dateFormat.format(cal.getTime());
            System.out.println("h: "+deadlineH);
            
            String deadlineDates;
            while(rs.next()){
//                tanggal = rs.getString("deadlineDate");
//                Date tglhslminus = dateFormat.parse(tanggal);
               
                
                deadlineDates = rs.getString("deadlineDate");
//                date = dateFormat.parse(deadlineDates);
//                Calendar date1 = null;
                
              
                if(deadlineH2.equals(deadlineDates) || deadlineH1.equals(deadlineDates) || deadlineH.equals(deadlineDates)){
                    String[] data ={rs.getString(1),rs.getString(2),rs.getString(3)};
                    tabel.addRow(data);
                }
                else{
                    System.out.println("blm jadi!");
                }
               
            }
//            String shw_total = Integer.toString(total);
//            show_total.setText(shw_total);
            rs.close();
            conn.close();
            }catch(SQLException ex){}
    }
  
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new deadlineCashOutView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel deadlineButton;
    private javax.swing.JTable deadlineTable;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel logoAnagement;
    private javax.swing.JLabel logoM;
    private javax.swing.JLabel logoM2;
    private javax.swing.JLabel logoOney;
    private javax.swing.JLabel logoutLabel;
    // End of variables declaration//GEN-END:variables
}
