
package Pengeluaran;

public class PengeluaranTidakDirencanakanController {
    private String dateBuy, cashOutName;
    private int charges;
    private PengeluaranTidakDirencanakanModel dataBaru;
    
    
    public PengeluaranTidakDirencanakanController() {}
    public PengeluaranTidakDirencanakanController(String des, int money, String date){
        this.cashOutName = des;
        this.charges = money;
        this.dateBuy = date;
        
        dataBaru = new PengeluaranTidakDirencanakanModel(des,money,date);
    }

    public String username(){
        String user = null;
        try{
            user = dataBaru.namaUser();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return user;
    }
   
    
    public void hapusSessUser(){
        try{
            dataBaru.hapusSessionUser();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void edit(int id){
         try{
            dataBaru.edit(id);
        }catch(Exception e){
            System.out.println(e.getMessage()+"controller"+"..."+id);
        }
    }
    public void insertData(){
         try{
            dataBaru.insert();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void delete(int id){
         try{
            dataBaru.delete(id);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
