/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login;

import java.sql.Connection;
import java.sql.SQLException;

public class loginController {
   
    loginModel logMod = new loginModel();
    Connection conn = null;
    public loginController(){
        conn = loginModel.connect(); 
    }

    public boolean hasilCekNamePass(String username,String password) throws SQLException
    {       
        boolean hasil = logMod.cekUsernamePassword(username, password);
        return hasil;
    } 
    public void sessionUserr(String username){
        logMod.sessionUser(username);
    }
   
}
