package Login;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author Ofri Cantika Valent
 */
public class loginModel {
     ResultSet rs = null;
     PreparedStatement ps = null;
    
      public static Connection connect(){
        String url = "jdbc:sqlite:E:/ofri/folder/SEMESTER_5/RPL/versi/it2_1/management.db";
        Connection conn = null;
        try{
            conn=DriverManager.getConnection(url);    
            
        }catch(SQLException e)
        {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    
    public boolean cekUsernamePassword(String username,String password) throws SQLException
    {
        
        Connection conn = null;
        conn = loginModel.connect();
        String sql = "select * from user where username =? and password =?";
        System.out.println(password);
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,username);
            ps.setString(2,password);
            rs = ps.executeQuery();

         
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
        conn.close();
        return rs.next();
    } 
   
     public void sessionUser(String username){
         Connection conn = null;
         conn = loginModel.connect();
         String sql = "INSERT INTO sessionUser(username) VALUES(?)";
         try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,username);
            ps.executeUpdate();
            conn.close();
         }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "con not insert to session user");
        }
     }
    
}
