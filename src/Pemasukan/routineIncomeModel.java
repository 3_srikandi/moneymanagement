
package Pemasukan;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import moneymanagement.MoneyManagement;

public class routineIncomeModel {
    private String diskripsi;
    private int uang;
    private String firstDate;
    private String lastDate;
    private String user;
    private int iddata;

    ResultSet rs = null;
    MoneyManagement koneksi = new MoneyManagement();
    
     public routineIncomeModel(){}
    public routineIncomeModel(String des, int money, String startDate, String endDate){
        this.diskripsi = des;
        this.uang = money;
        this.firstDate = startDate;
        this.lastDate = endDate;
    }
   public String namaUser(){
     String dataUser = null;
        try(Connection conn = koneksi.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            
//            username.close();
           conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser ggl");
        }
        return dataUser;
 }
    public void insert(){
         String datauser = namaUser();
        try(Connection conn = koneksi.connect()){
            
            
            String sqlQuery = "INSERT INTO incomeRutin(startDate, endDate, description, money, username) values(?,?,?,?,?)";
            try( PreparedStatement kalimat = conn.prepareStatement(sqlQuery)){              
                 kalimat.setString(1, this.firstDate);
                 kalimat.setString(2, this.lastDate);
                 kalimat.setString(3, this.diskripsi);
                 kalimat.setInt(4, this.uang);
                 kalimat.setString(5, datauser);
                 kalimat.executeUpdate();
//                 kalimat.close();
                 JOptionPane.showMessageDialog(null, "Data is saved"); 
                 
            }
            
            conn.close();
        }catch(Exception e){
                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
  
 
 public void hapusSessionUser() {
      
        String datauser = namaUser() ;
        try(Connection conn = koneksi.connect()){
            
            String sql = "DELETE FROM sessionUser WHERE username = '"+datauser+"'";
           
            try(PreparedStatement st = conn.prepareStatement(sql)){
               
                st.executeUpdate();
//                JOptionPane.showMessageDialog(null, "can delete model");
//                st.close();
            }catch(Exception e){
//                JOptionPane.showMessageDialog(null, "can not delete model");
            }
            conn.close();
        }catch(Exception e){
//                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
 
 
// public int idDataDelete() {
//      String user = namaUser(); int idUser = 0;
//      System.out.println(user+firstDate+lastDate+diskripsi+uang);
//      ResultSet IDUSER = null;
//      try(Connection conn = koneksi.connect()){
//        
//        IDUSER = conn.createStatement().executeQuery("SELECT id FROM incomeRutin WHERE startDate = '"+this.firstDate+"' and endDate = '"+this.lastDate+"' and description = '"+this.diskripsi+"'  and money = '"+this.uang+"' and username = '"+user+"'");
//        while( IDUSER.next()){
//            idUser = IDUSER.getInt("id");
//            return idUser;
//        }   
//  
//        IDUSER.close();
//        conn.close();
//      }catch(Exception e){
//          System.out.println(e+"id model gagal ");
//      }
//      return idUser;
// }
 
 public void deleteData(int idData) {
    
            //int id = idDataDelete();
            System.out.println(idData+firstDate+lastDate+diskripsi+uang);
            String sql = "DELETE FROM incomeRutin WHERE id = '"+idData+"'";
        
            try(Connection conn = koneksi.connect(); PreparedStatement st = conn.prepareStatement(sql)){
                st.executeUpdate();
            
//                conn.close();
            }catch(Exception e){
                System.out.println(e+"delete model ggl");
            }
     
}
 public void editData(int idData) {
        String nama_user = namaUser();
        System.out.println(idData+firstDate+lastDate+diskripsi+uang);
        String sql = "UPDATE incomeRutin SET startDate = ?, endDate = ? , description = ?  , money = ?  WHERE id = ? ";
          
        try(Connection conn = koneksi.connect()){
                PreparedStatement st = conn.prepareStatement(sql);
                st.setString(1, this.firstDate);
                st.setString(2, this.lastDate);
                st.setString(3, this.diskripsi);
                st.setInt(4, this.uang);
                st.setInt(5, idData);
                st.executeUpdate();
//                st.close();
//                conn.close();
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
 }
}
