package Pemasukan;

import javax.swing.JOptionPane;

public class unroutineIncomeController {
    private String description;
    private int money;
    private String startDate;
    private unroutineIncomeModel dataBaru;
    
    public unroutineIncomeController(String des, int money, String startDate){
        this.description = des;
        this.money = money;
        this.startDate = startDate;
        
        dataBaru = new unroutineIncomeModel(des,money,startDate);
    }
    
    public unroutineIncomeController(){}
    
    public void inserttoDb(){
        try{  
           dataBaru.insert();
                          
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to access!");
        }
    }
    
    public void delete(int idData){
         try{
            dataBaru.deleteData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
     }
    
    public void edit(int idData){
         try{
            dataBaru.editData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to edit controller");
        }
     }
    
    public void hapusUser(){
        unroutineIncomeModel sendUser = new unroutineIncomeModel();
        try{
            sendUser.hapusSessionUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
    
    public String namaUSer(){
         unroutineIncomeModel sendUser = new unroutineIncomeModel();
         try {
            String nama = sendUser.namaUser();
            return nama;
         }catch(Exception e){
             
         }
         return "";
     }
}
