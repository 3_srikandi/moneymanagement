package Pemasukan;

import javax.swing.JOptionPane;

public class routineIncomeController {
    private String description;
    private int money;
    private String startDate;
    private String endDate;
    private routineIncomeModel dataBaru;
    
   
    public routineIncomeController(String des, int money, String startDate, String endDate){
        this.description = des;
        this.money = money;
        this.startDate = startDate;
        this.endDate = endDate;
        
        dataBaru = new routineIncomeModel(des,money,startDate, endDate);
    }

    public routineIncomeController(){}
    
    public void inserttoDb(){
         try{  
                dataBaru.insert();
                          
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, "Can not to access!");
            }
    }
    
     public void hapusUser(){
        routineIncomeModel sendUser = new routineIncomeModel();
        try{
            sendUser.hapusSessionUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
    public String namaUSer(){
         routineIncomeModel sendUser = new routineIncomeModel();
         try {
            String nama = sendUser.namaUser();
            return nama;
         }catch(Exception e){
             
         }
         return "";
     }
     
    public void delete(int idData){
         try{
            dataBaru.deleteData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
     }
     
    public void edit(int idData){
         try{
            dataBaru.editData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to edit controller");
        }
     }
}
