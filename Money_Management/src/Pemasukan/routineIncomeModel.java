
package Pemasukan;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import money_management.Money_Management;

public class routineIncomeModel {
    private String startDate;
    private String endDate;
    private String description;
    private int charge;
    private String user;
    private int iddata;

    ResultSet rs = null;
    Money_Management koneksi = new Money_Management();
    
    public routineIncomeModel(){}
    
    public routineIncomeModel(String start, String end, String des, int charge){
        this.startDate = start;
        this.endDate = end;
        this.description = des;
        this.charge = charge;
    }
    
    public String namaUser(){
    String dataUser = null;
        try(Connection conn = koneksi.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            
           conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser ggl");
        }
        return dataUser;
    }
    
    public void insert(){
        String datauser = namaUser();
        try(Connection conn = koneksi.connect()){
            
            
            String sqlQuery = "INSERT INTO incomeRutin(startDate, endDate, description, money, username) values(?,?,?,?,?)";
            try( PreparedStatement kalimat = conn.prepareStatement(sqlQuery)){              
                 kalimat.setString(1, this.startDate);
                 kalimat.setString(2, this.endDate);
                 kalimat.setString(3, this.description);
                 kalimat.setInt(4, this.charge);
                 kalimat.setString(5, datauser);
                 kalimat.executeUpdate();
//                 kalimat.close();
                 JOptionPane.showMessageDialog(null, "Data is saved"); 
                 
            }
            
            conn.close();
        }catch(Exception e){
                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
  
 
    public void hapusSessionUser() {
      
        String datauser = namaUser() ;
        try(Connection conn = koneksi.connect()){
            
            String sql = "DELETE FROM sessionUser WHERE username = '"+datauser+"'";
           
            try(PreparedStatement st = conn.prepareStatement(sql)){
               
                st.executeUpdate();
            }catch(Exception e){
            }
            conn.close();
        }catch(Exception e){ } 
    }
 
 
 
    public void deleteData(int idData) {
            System.out.println(idData+startDate+endDate+description+charge);
            String sql = "DELETE FROM incomeRutin WHERE id = '"+idData+"'";
        
            try(Connection conn = koneksi.connect(); PreparedStatement st = conn.prepareStatement(sql)){
                st.executeUpdate();
            }catch(Exception e){
                System.out.println(e+"delete model ggl");
            }
     
    }
    public void editData(int idData) {
        String nama_user = namaUser();
        System.out.println(idData+startDate+endDate+description+charge);
        String sql = "UPDATE incomeRutin SET startDate = ?, endDate = ? , description = ?  , money = ?  WHERE id = ? ";
          
        try(Connection conn = koneksi.connect()){
                PreparedStatement st = conn.prepareStatement(sql);
                st.setString(1, this.startDate);
                st.setString(2, this.endDate);
                st.setString(3, this.description);
                st.setInt(4, this.charge);
                st.setInt(5, idData);
                st.executeUpdate();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
