package Pemasukan;

import javax.swing.JOptionPane;

public class unroutineIncomeController {
    private String startDate;
    private String description;
    private int charge;
    private unroutineIncomeModel dataBaru;
    
    public unroutineIncomeController(String start, String des, int charge){
        this.startDate = start;
        this.description = des;
        this.charge = charge;
        
        dataBaru = new unroutineIncomeModel(start, des, charge);
    }
    
    public unroutineIncomeController(){}
    
    public void inserttoDb(){
        try{  
           dataBaru.insert();
                          
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to access!");
        }
    }
    
    public void delete(int idData){
         try{
            dataBaru.deleteData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
     }
    
    public void edit(int idData){
         try{
            dataBaru.editData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to edit controller");
        }
     }
    
    public void hapusUser(){
        unroutineIncomeModel sendUser = new unroutineIncomeModel();
        try{
            sendUser.hapusSessionUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
    
    public String namaUSer(){
         unroutineIncomeModel sendUser = new unroutineIncomeModel();
         try {
            String nama = sendUser.namaUser();
            return nama;
         }catch(Exception e){
             
         }
         return "";
     }
}
