package Pemasukan;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import money_management.Money_Management;

public class unroutineIncomeModel {
    private String startDate;
    private String description;
    private int charge;
    
   
    Money_Management koneksi = new Money_Management();
    
    public unroutineIncomeModel(){}
    
    public unroutineIncomeModel(String start, String des, int charge){
        this.startDate = start;
        this.description = des;
        this.charge = charge;
    }
    
     public void insert(){
        String datauser;
        try(Connection conn = koneksi.connect()){
            ResultSet username = conn.createStatement().executeQuery("select username from sessionUser");
            username.next();
            datauser = username.getString("username");
            String sqlQuery = "INSERT INTO unroutineIncome(startDate, description,totalUang, username) values(?,?,?,?)";
            
            try( PreparedStatement kalimat = conn.prepareStatement(sqlQuery)){              
                 kalimat.setString(1, this.startDate);
                 kalimat.setString(2, this.description);
                 kalimat.setInt(3, this.charge);
                 kalimat.setString(4, datauser);
                 kalimat.executeUpdate();
                 JOptionPane.showMessageDialog(null, "Data is saved");  
            }

        }catch(Exception e){
                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
     
     public void hapusSessionUser() {
      
        String datauser;
        try(Connection conn = koneksi.connect()){
            ResultSet username = conn.createStatement().executeQuery("select username from sessionUser");
            username.next();
            datauser = username.getString("username");
            System.out.println(datauser);
            String sql = "DELETE FROM sessionUser WHERE username = '"+datauser+"'";
           
            try(PreparedStatement st = conn.prepareStatement(sql)){
                st.executeUpdate();
                 JOptionPane.showMessageDialog(null, "can delete model");
//                 
            }catch(Exception e){
//                JOptionPane.showMessageDialog(null, "can not delete model");
            }

        }catch(Exception e){
//                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
     
    public String namaUser(){
     String dataUser = null;
        try(Connection conn = koneksi.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            
//            username.close();
           conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser ggl");
        }
        return dataUser;
        
    }
    
    public void deleteData(int idData) {
    
            //int id = idDataDelete();
            System.out.println(idData+startDate+description+charge);
            String sql = "DELETE FROM unroutineIncome WHERE id = '"+idData+"'";
        
            try(Connection conn = koneksi.connect(); PreparedStatement st = conn.prepareStatement(sql)){
                st.executeUpdate();
            
//                conn.close();
            }catch(Exception e){
                System.out.println(e+"delete model ggl");
            }
     
    }
    public void editData(int idData) {
        String nama_user = namaUser();
        System.out.println(idData+startDate+description+charge);
        String sql = "UPDATE unroutineIncome SET startDate = ?, description = ?  , totalUang = ?  WHERE id = ? ";
          
        try(Connection conn = koneksi.connect()){
                PreparedStatement st = conn.prepareStatement(sql);
                st.setString(1, this.startDate);
                st.setString(2, this.description);
                st.setInt(3, this.charge);
                st.setInt(4, idData);
                st.executeUpdate();
//                st.close();
//                conn.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
