package Pemasukan;

import javax.swing.JOptionPane;

public class routineIncomeController {
    private String startDate;
    private String endDate;
    private String description;
    private int charge;
    private routineIncomeModel dataBaru;
    
   
    public routineIncomeController(String start, String end, String des, int charge){
        this.startDate = start;
        this.endDate = end;
        this.description = des;
        this.charge = charge;
        dataBaru = new routineIncomeModel(start, end, des, charge);
    }

    public routineIncomeController(){}
    
    public void inserttoDb(){
         try{  
                dataBaru.insert();
                          
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, "Can not to access!");
            }
    }
    
     public void hapusUser(){
        routineIncomeModel sendUser = new routineIncomeModel();
        try{
            sendUser.hapusSessionUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
    public String namaUSer(){
         routineIncomeModel sendUser = new routineIncomeModel();
         try {
            String nama = sendUser.namaUser();
            return nama;
         }catch(Exception e){
             
         }
         return "";
    }
     
    public void delete(int idData){
         try{
            dataBaru.deleteData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
     
    public void edit(int idData){
         try{
            dataBaru.editData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to edit controller");
        }
    }
}
