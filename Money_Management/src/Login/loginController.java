
package Login;

import java.sql.Connection;
import java.sql.SQLException;

public class loginController {
   
    loginModel logMod = new loginModel();
    Connection conn = null;
    public loginController(){
        conn = loginModel.connect(); 
    }

    public boolean hasilCekNamePass(String username,String password) throws SQLException
    {       
        boolean hasil = logMod.cekUsernamePassword(username, password);
        return hasil;
    } 
    public void sessionUserr(String username){
        logMod.sessionUser(username);
    }
   
}
