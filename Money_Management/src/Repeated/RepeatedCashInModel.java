/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repeated;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import moneymanagement.MoneyManagement;

/**
 *
 * @author Ofri Cantika Valent
 */
public class RepeatedCashInModel {
//    private  String description;
//    private  int money;
//    private  String start_Date;
//    private  String end_Date;
    MoneyManagement koneksi = new MoneyManagement();
//    
//    public RepeatedCashInModel(){}
//    public RepeatedCashInModel(String descripUser, int moneyUser, String startDate, String endDate){
//        this.description = descripUser;
//        this.money = moneyUser;
//        this.start_Date = startDate;
//        this.end_Date = endDate;
//    }
    public String namaUser(){
        String dataUser = null;
        try(Connection conn = koneksi.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");

           conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser ggl");
        }
        return dataUser;
    }
     public void insert(String descripUser, int money, String start, String end){
         String datauser = namaUser();
         
        try(Connection conn = koneksi.connect()){
            System.out.println(descripUser+money+start+end);
            String sqlQuery = "INSERT INTO incomeRutin(startDate, endDate, description, money, username) values(?,?,?,?,?)";
            try( PreparedStatement kalimat = conn.prepareStatement(sqlQuery)){              
                 kalimat.setString(1, start);
                 kalimat.setString(2, end);
                 kalimat.setString(3, descripUser);
                 kalimat.setInt(4, money);
                 kalimat.setString(5, datauser);
                 kalimat.executeUpdate();

                 JOptionPane.showMessageDialog(null, "Data is saved"); 
                 
            }
            conn.close();
        }catch(Exception e){
                JOptionPane.showMessageDialog(null, "can not select model ");
        } 
    }
    
}
