/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pengeluaran;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import money_management.Money_Management;

/**
 *
 * @author Cyndy Alisia
 */
public class plannedCashOutModel {
    private String deadlineDate;
    private String cashOutName;
    private int charge;
    private String user;
    private int iddata;
    ResultSet rs = null;
    Money_Management koneksi = new Money_Management();
    
    public plannedCashOutModel(){}
    
    public plannedCashOutModel(String date, String name, int charge){
        this.deadlineDate = date;
        this.cashOutName = name;
        this.charge = charge;
    }

    public String namaUser(){
    String dataUser = null;
        try(Connection conn = koneksi.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser ggl");
        }
        return dataUser;
    }
    
    public void insert(){
        String datauser = namaUser();
        String sqlQuery = "INSERT INTO planCashOut(deadlineDate, cashOutName, charge, username) values(?,?,?,?)";
        try(Connection conn = koneksi.connect();
            PreparedStatement kalimat = conn.prepareStatement(sqlQuery)){              
                kalimat.setString(1, this.deadlineDate);
                kalimat.setString(2, this.cashOutName);
                kalimat.setInt(3, this.charge);
                kalimat.setString(4, datauser);
                kalimat.executeUpdate();
                JOptionPane.showMessageDialog(null, "Data is saved"); 
                
            conn.close();
        }catch(Exception e){
            //JOptionPane.showMessageDialog(null, "can not select model ");
            System.out.println(e.getMessage());
            
        } 
    }
  
 
    public void hapusSessionUser() {

            String datauser = namaUser() ;
            try(Connection conn = koneksi.connect()){

                String sql = "DELETE FROM sessionUser WHERE username = '"+datauser+"'";

                try(PreparedStatement st = conn.prepareStatement(sql)){

                    st.executeUpdate();
                    
                }catch(Exception e){
    
                }
                conn.close();
            }catch(Exception e){
    
            } 
    }

  
    public void deleteData(int idData) {
        String sql = "DELETE FROM planCashOut WHERE id = '"+idData+"'";

                try(Connection conn = koneksi.connect(); PreparedStatement st = conn.prepareStatement(sql)){
                    st.executeUpdate();
    //                conn.close();
                }catch(Exception e){
                    System.out.println(e+"delete model ggl");
                    System.out.println(e.getMessage());
                }
    }
    
    public void editData(int idData) {
        String nama_user = namaUser();
        System.out.println(idData+deadlineDate+cashOutName+charge);
        String sql = "UPDATE planCashOut SET deadlineDate = ?, cashOutName = ? , charge = ?  WHERE id = ? ";

        try(Connection conn = koneksi.connect()){
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, this.deadlineDate);
            st.setString(2, this.cashOutName);
            st.setInt(3, this.charge);
            st.setInt(4, idData);
            st.executeUpdate();
        }catch(Exception e){
               System.out.println(e.getMessage());
        }
   }

}
