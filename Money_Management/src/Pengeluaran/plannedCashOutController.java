/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pengeluaran;

import javax.swing.JOptionPane;

/**
 *
 * @author Cyndy Alisia
 */
public class plannedCashOutController {
    private String deadlineDate;
    private String cashOutName;
    private int charge;
    private plannedCashOutModel dataBaru;
    
   
    public plannedCashOutController(String date, String name, int charge){
        this.deadlineDate = date;
        this.cashOutName = name;
        this.charge = charge;
        dataBaru = new plannedCashOutModel(date, name, charge);
    }

    public plannedCashOutController(){}
    
    public void inserttoDb(){
         try{  
                dataBaru.insert();
                          
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, "Can not to access!");
            }
    }
    
     public void hapusUser(){
        plannedCashOutModel sendUser = new plannedCashOutModel();
        try{
            sendUser.hapusSessionUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
        }
    }
     
    public String namaUSer(){
        plannedCashOutModel sendUser = new plannedCashOutModel();
         try {
            String nama = sendUser.namaUser();
            return nama;
         }catch(Exception e){
             
         }
         return "";
     }
     
    public void deleteData(int idData){
         try{
            dataBaru.deleteData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to delete controller");
             System.out.println(e.getMessage());
        }
     }
     
    public void edit(int idData){
         try{
            dataBaru.editData(idData);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Can not to edit controller");
        }
     }

    
}
