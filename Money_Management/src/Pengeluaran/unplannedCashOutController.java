
package Pengeluaran;

public class unplannedCashOutController {
    private String dateBuy, cashOutName;
    private int charges;
    private unplannedCashOutModel dataBaru;
    
    
    public unplannedCashOutController() {}
    public unplannedCashOutController(String des, int money, String date){
        this.cashOutName = des;
        this.charges = money;
        this.dateBuy = date;
        
        dataBaru = new unplannedCashOutModel(des,money,date);
    }

    public String username(){
        String user = null;
        try{
            user = dataBaru.namaUser();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return user;
    }
   
    
    public void hapusSessUser(){
        try{
            dataBaru.hapusSessionUser();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void edit(int id){
         try{
            dataBaru.edit(id);
        }catch(Exception e){
            System.out.println(e.getMessage()+"controller"+"..."+id);
        }
    }
    public void insertData(){
         try{
            dataBaru.insert();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void deleteData(int id){
         try{
            dataBaru.deleteData(id);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
