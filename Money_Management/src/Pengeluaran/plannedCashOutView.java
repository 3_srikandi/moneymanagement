package Pengeluaran;

import Login.loginView;
import homePage.homePageFrameView;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import money_management.Money_Management;

public class plannedCashOutView extends javax.swing.JFrame {
    private DefaultTableModel tabel;
    Money_Management koneksi = new Money_Management();
    int idData;
    int total = 0;
    
    public plannedCashOutView() {
        
        initComponents();
        String nama = namaUser();
        lbl_nama_user.setText(nama);
        String[] judul = {"deadLineDate","cashOutName","charge"};
        tabel = new DefaultTableModel(judul, 0);
        tableData.setModel(tabel);
        tampil();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        label_title1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        label_title2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        logout = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        deadlineDate = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        cashOutName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        Charge = new javax.swing.JTextField();
        addData = new javax.swing.JButton();
        editData = new javax.swing.JButton();
        deleteData = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableData = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        showTotal = new javax.swing.JLabel();
        searchData = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        lbl_nama_user = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 255, 173));

        label_title1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_title1.setText("oney");
        label_title1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_title1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_title1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_title1MouseExited(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 102, 51));
        jLabel4.setText("M");
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel4MouseExited(evt);
            }
        });

        label_title2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_title2.setText("anagement");
        label_title2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                label_title2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label_title2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label_title2MouseExited(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Cooper Black", 0, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 159, 92));
        jLabel3.setText("M");
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel3MouseExited(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel2.setText("Planned Cash-Out");

        logout.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        logout.setText("   Logout");
        logout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoutMouseExited(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel12.setText("Deadline Date           :");

        deadlineDate.setDateFormatString("yyyy-MMM-dd");

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel1.setText("Cash-Out Name         :");

        cashOutName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cashOutNameActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel5.setText("Charge                    :");

        addData.setBackground(new java.awt.Color(1, 169, 182));
        addData.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        addData.setText("Add");
        addData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                addDataMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                addDataMouseExited(evt);
            }
        });
        addData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addDataActionPerformed(evt);
            }
        });

        editData.setBackground(new java.awt.Color(1, 169, 182));
        editData.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        editData.setText("Edit");
        editData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                editDataMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                editDataMouseExited(evt);
            }
        });
        editData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editDataActionPerformed(evt);
            }
        });

        deleteData.setBackground(new java.awt.Color(1, 169, 182));
        deleteData.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        deleteData.setText("Delete");
        deleteData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                deleteDataMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                deleteDataMouseExited(evt);
            }
        });
        deleteData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteDataActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(153, 255, 173));

        jLabel6.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel6.setText("Detail Pengeluaran Direncanakan");

        tableData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No Code", "Deadline Date", "Cash-Out Name", "Charge"
            }
        ));
        tableData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableDataMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tableData);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 719, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(259, 259, 259)
                        .addComponent(jLabel6)))
                .addContainerGap(308, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addGap(4, 4, 4)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE))
        );

        jLabel7.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel7.setText("Total Planed Cash-Out  : Rp");

        showTotal.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        showTotal.setText("       ");

        searchData.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchDataKeyReleased(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel8.setText("Search :");

        lbl_nama_user.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel9.setText("Hi, ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_title1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(label_title2)
                                .addGap(279, 279, 279)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbl_nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(logout, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(461, 461, 461)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(editData, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(addData, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(deleteData, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(246, 246, 246)
                        .addComponent(jLabel2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deadlineDate, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cashOutName, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Charge)))
                        .addGap(772, 772, 772))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(showTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(88, 88, 88)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(searchData, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(label_title1)
                                .addComponent(jLabel4)
                                .addComponent(label_title2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbl_nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(logout, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(85, 85, 85)
                        .addComponent(addData, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(95, 95, 95)
                        .addComponent(jLabel2)
                        .addGap(39, 39, 39)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(deadlineDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(cashOutName, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(editData, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(deleteData, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(Charge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(searchData, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(showTotal)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 784, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void deleteDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteDataActionPerformed
        try{
            
            String name = cashOutName.getText();
            String date =((JTextField)deadlineDate.getDateEditor().getUiComponent()).getText();
            int money =  Integer.parseInt(Charge.getText());
          
            plannedCashOutController del_data = new plannedCashOutController(date, name, money);
            del_data.deleteData(idData);
            System.out.println(idData);
            cashOutName.setText("");
            ((JTextField)deadlineDate.getDateEditor().getUiComponent()).setText("");
            Charge.setText("");
            tampil();
            
        }catch(Exception e){
             System.out.println(e);         
        }
    }//GEN-LAST:event_deleteDataActionPerformed

    private void deleteDataMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteDataMouseExited
        deleteData.setForeground(Color.black);
    }//GEN-LAST:event_deleteDataMouseExited

    private void deleteDataMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteDataMouseEntered
        deleteData.setForeground(Color.white);
    }//GEN-LAST:event_deleteDataMouseEntered

    private void editDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editDataActionPerformed
         try{            
            String name = cashOutName.getText();
            String date =((JTextField)deadlineDate.getDateEditor().getUiComponent()).getText();
            int money =  Integer.parseInt(Charge.getText());
               
            plannedCashOutController del_data = new plannedCashOutController(date, name, money);
            del_data.edit(idData);
            System.out.println(idData);
            cashOutName.setText("");
            ((JTextField)deadlineDate.getDateEditor().getUiComponent()).setText("");
            Charge.setText("");
            tampil();
        }catch(Exception e){
            System.out.println(e.getMessage());         
        }
    }//GEN-LAST:event_editDataActionPerformed

    private void editDataMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editDataMouseExited
        editData.setForeground(Color.black);
    }//GEN-LAST:event_editDataMouseExited

    private void editDataMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editDataMouseEntered
        editData.setForeground(Color.white);
    }//GEN-LAST:event_editDataMouseEntered

    private void addDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addDataActionPerformed
        // TODO add your handling code here:
        boolean total = false;
        DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
        Date sekarang = new Date();

        String startString = df1.format(sekarang);
        try {
            sekarang = df1.parse(startString);
        } catch (ParseException ex) {
            
        }

        if(((JTextField)deadlineDate.getDateEditor().getUiComponent()).getText().equals("") && cashOutName.getText().equals("") && Charge.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Pastikan semua telah terisi !");

        }
        else if(((JTextField)deadlineDate.getDateEditor().getUiComponent()).getText().equals("")){
            JOptionPane.showMessageDialog(null, "Deadline date can not empty!");
        }
        else if(cashOutName.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Cash-Out Name can not empty!");
        }
        else if(Charge.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Charge can not empty!");
        }
        else if(-1 == deadlineDate.getDate().compareTo(sekarang)){
            JOptionPane.showMessageDialog(null, "Deadline can not be smaller with the current date");
        }
        else{
            try{
                String nama = cashOutName.getText();
                String tanggal =((JTextField)deadlineDate.getDateEditor().getUiComponent()).getText();
                int money =  Integer.parseInt(Charge.getText());
                plannedCashOutController databaru = new plannedCashOutController(tanggal, nama, money);
                databaru.inserttoDb();
                cashOutName.setText("");
                ((JTextField)deadlineDate.getDateEditor().getUiComponent()).setText("");
                Charge.setText("");
                tampil();
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, "Data lost");
            }
        }

    }//GEN-LAST:event_addDataActionPerformed

    private void addDataMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addDataMouseExited
        addData.setForeground(Color.black);
    }//GEN-LAST:event_addDataMouseExited

    private void addDataMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addDataMouseEntered
        addData.setForeground(Color.white);
    }//GEN-LAST:event_addDataMouseEntered

    private void cashOutNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cashOutNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cashOutNameActionPerformed

    private void logoutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseExited
        logout.setForeground(Color.black);
    }//GEN-LAST:event_logoutMouseExited

    private void logoutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseEntered
        logout.setForeground(Color.blue);
    }//GEN-LAST:event_logoutMouseEntered

    private void logoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseClicked
        plannedCashOutController deleteuser = new plannedCashOutController();
        try{
            deleteuser.hapusUser();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "can not logout");
        }
        new loginView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logoutMouseClicked

    private void jLabel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseExited
        label_title2.setForeground(Color.black);
        label_title1.setForeground(Color.black);
    }//GEN-LAST:event_jLabel3MouseExited

    private void jLabel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseEntered
        label_title2.setForeground(Color.white);
        label_title1.setForeground(Color.white);
    }//GEN-LAST:event_jLabel3MouseEntered

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        new homePageFrameView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jLabel3MouseClicked

    private void label_title2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title2MouseExited
        label_title2.setForeground(Color.black);
        label_title1.setForeground(Color.black);
    }//GEN-LAST:event_label_title2MouseExited

    private void label_title2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title2MouseEntered
        label_title2.setForeground(Color.white);
        label_title1.setForeground(Color.white);
    }//GEN-LAST:event_label_title2MouseEntered

    private void label_title2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title2MouseClicked
        new homePageFrameView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_label_title2MouseClicked

    private void jLabel4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseExited
        label_title2.setForeground(Color.black);
        label_title1.setForeground(Color.black);
    }//GEN-LAST:event_jLabel4MouseExited

    private void jLabel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseEntered
        label_title2.setForeground(Color.white);
        label_title1.setForeground(Color.white);
    }//GEN-LAST:event_jLabel4MouseEntered

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        new homePageFrameView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jLabel4MouseClicked

    private void label_title1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title1MouseExited
        label_title2.setForeground(Color.black);
        label_title1.setForeground(Color.black);
    }//GEN-LAST:event_label_title1MouseExited

    private void label_title1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title1MouseEntered
        label_title2.setForeground(Color.white);
        label_title1.setForeground(Color.white);
    }//GEN-LAST:event_label_title1MouseEntered

    private void label_title1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_title1MouseClicked
        new homePageFrameView().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_label_title1MouseClicked

    private void searchDataKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchDataKeyReleased
        String query = searchData.getText().toLowerCase();
        filter(query);
    }//GEN-LAST:event_searchDataKeyReleased

    private void tableDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableDataMouseClicked
        DefaultTableModel model = (DefaultTableModel)tableData.getModel();
        int row = tableData.getSelectedRow();
       
        ((JTextField)deadlineDate.getDateEditor().getUiComponent()).setText(model.getValueAt(row,0).toString());
        cashOutName.setText(model.getValueAt(row,1).toString());
        Charge.setText(model.getValueAt(row,2).toString());
        
        String dsc = cashOutName.getText();
        String date =((JTextField)deadlineDate.getDateEditor().getUiComponent()).getText();
        int money =  Integer.parseInt(Charge.getText());
       
       //PengeluaranTidakDirencanakanController getUsername = new PengeluaranTidakDirencanakanController();
       String user = namaUser();
        try(Connection conn = koneksi.connect()){ 
                ResultSet IDUSER = conn.createStatement().executeQuery("SELECT id FROM planCashOut WHERE deadlineDate = '"+date+"' and cashOutName = '"+dsc+"'  and charge = '"+money+"' and username = '"+user+"'");
                while( IDUSER.next()){
                    idData = IDUSER.getInt("id");
                }   
        }catch(Exception e){
          System.out.println(e+"id view gagal ");
        }
    }//GEN-LAST:event_tableDataMouseClicked
    public String namaUser(){
     String dataUser = null;
        try(Connection conn = koneksi.connect()){
            Statement stmt = conn.createStatement();
            ResultSet username = stmt.executeQuery("select username from sessionUser");
            username.next();
            dataUser = username.getString("username");
            
//            username.close();
           conn.close();
            
        }catch(Exception e){
            System.out.println(e+"nmuser view ggl");
        }
        return dataUser;
    }
    
    public void filter(String query){
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(tabel);
        tableData.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(query));
    }
    
    public void tampil() {
        int row = tableData.getRowCount();
        total = 0;
        for(int s = 0; s<row;s++){
            tabel.removeRow(0);
        }
        plannedCashOutController userActiv = new plannedCashOutController();
        String user = userActiv.namaUSer();
        String sqlQuery = "SELECT deadlineDate, cashOutName, charge FROM planCashOut where username = ? ";
            
        try(Connection conn = koneksi.connect();
            PreparedStatement ps = conn.prepareStatement(sqlQuery);){
            
            ps.setString(1, user);
            
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                
                total = total + rs.getInt(3);
                String[] data ={rs.getString("deadlineDate"),rs.getString("cashOutName"),Integer.toString(rs.getInt("charge"))};
                tabel.addRow(data);
            }
            String shw_total = Integer.toString(total);
            showTotal.setText(shw_total);
            conn.close();
            
        }catch(Exception e){
            //JOptionPane.showMessageDialog(null, "can not select model ");
            System.out.println(e.getMessage());
        }
    }
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new plannedCashOutView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Charge;
    private javax.swing.JButton addData;
    private javax.swing.JTextField cashOutName;
    private com.toedter.calendar.JDateChooser deadlineDate;
    private javax.swing.JButton deleteData;
    private javax.swing.JButton editData;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel label_title1;
    private javax.swing.JLabel label_title2;
    private javax.swing.JLabel lbl_nama_user;
    private javax.swing.JLabel logout;
    private javax.swing.JTextField searchData;
    private javax.swing.JLabel showTotal;
    private javax.swing.JTable tableData;
    // End of variables declaration//GEN-END:variables
}
